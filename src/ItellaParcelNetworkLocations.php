<?php

class ItellaParcelNetworkLocations extends ParcelNetworkLocations
{
    protected $dataType = 'xml';

    protected $dataUrl = 'http://www.smartpost.ee/places.xml';

    public function extractByParameters(bool $active = true)
    {
        $result = [];

        foreach ($this->getData()['place'] as $parcelLocker) {

            if ($active && $parcelLocker['active'] !== "1") {
                continue;
            }

            $result[] = $parcelLocker;
        }

        return $result;
    }

    public function createMap(array $data)
    {
        $result = [];

        foreach ($data as $parcelNetworkItem) {
            $addressGroupArray = [
                $parcelNetworkItem['group_name'],
                $parcelNetworkItem['city']
            ];

            $parcelNetworkItemData = [
                'name' => $parcelNetworkItem['name'],
                'address' => $parcelNetworkItem['address'],
                'place_id' => $parcelNetworkItem['place_id'],
            ];

            $result = $this->insertUsingKeys($result, $addressGroupArray, $parcelNetworkItemData);
        }

        return $result;
    }
}