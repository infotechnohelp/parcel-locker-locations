<?php

class OmnivaParcelNetworkLocations extends ParcelNetworkLocations
{
    protected $dataType = 'json';

    protected $dataUrl = 'https://www.omniva.ee/locations.json';


    public function extractByParameters(int $type = null, string $countryCode = "EE")
    {
        $result = [];

        foreach ($this->getData() as $parcelLocker) {

            if ($parcelLocker['A0_NAME'] !== $countryCode || $type !== null && $parcelLocker['TYPE'] !== (string)$type) {
                continue;
            }

            $result[] = $parcelLocker;
        }

        return $result;
    }

    public function createMap(array $data)
    {
        $result = [];

        foreach ($data as $parcelNetworkItem) {

            $addressGroupArray = [];

            for ($i = 1; $i <= 3; $i++) {
                if ($parcelNetworkItem["A{$i}_NAME"] !== "NULL") {
                    $addressGroupArray[] = $parcelNetworkItem["A{$i}_NAME"];
                }
            }

            $detailedAddress = [];

            for ($i = 4; $i <= 8; $i++) {
                if ($parcelNetworkItem["A{$i}_NAME"] !== "NULL") {
                    $detailedAddress[] = $parcelNetworkItem["A{$i}_NAME"];
                }
            }

            $parcelNetworkItemData = [
                'name' => $parcelNetworkItem['NAME'],
                'address' => $detailedAddress,
                'zip' => $parcelNetworkItem['ZIP'],
            ];

            $result = $this->insertUsingKeys($result, $addressGroupArray, $parcelNetworkItemData);

        }

        return $result;
    }
}