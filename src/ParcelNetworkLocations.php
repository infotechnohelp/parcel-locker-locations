<?php

abstract class ParcelNetworkLocations
{
    public /*protected*/ function getData()
    {
        $data = file_get_contents($this->dataUrl);

        switch ($this->dataType) {
            case "json":
                return json_decode($data, true);
                break;
            case "xml":
                $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
                return json_decode(json_encode($xml), true);
                break;
            case "csv":
                $data = iconv($this->csvEncoding, "UTF-8", html_entity_decode( $data ,ENT_COMPAT,'utf-8'));

                $lines = explode("\n", $data);

                $result = [];

                foreach($lines as $line){
                    $rows = explode($this->csvDelimiter, $line);
                    if(empty($rows) || count($rows) === 1 && empty($rows[0])){
                        continue;
                    }
                    $result[] = $rows;
                }

                return $result;
                break;
            default:
                return $data;
        }
    }

    protected function insertUsingKeys($arr, $keys, $value)
    {

        $a = &$arr;

        while (count($keys) > 0) {
            $k = array_shift($keys);

            if (!is_array($a)) {
                $a = array();
            }

            $a = &$a[$k];
        }

        $a[] = $value;

        return $arr;
    }

    public function createMap(array $data)
    {
    }
}