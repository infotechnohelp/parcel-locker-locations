<?php

class DpdParcelNetworkLocations extends ParcelNetworkLocations
{
    protected $dataType = 'csv';

    protected $dataUrl = "ftp://ftp.dpd.ee/parcelshop/psexport_latest.csv";

    protected $csvDelimiter = "|";

    protected $csvEncoding = "WINDOWS-1257";


    public function extractByParameters(string $countryCode = "EE")
    {
        $result = [];

        foreach ($this->getData() as $parcelLocker) {
            $code = $parcelLocker[22];

            if(strpos($code, $countryCode) !== 0){
                continue;
            }

            list($paketshopId, $depot, $firma, $address, $zip, $ort) = $parcelLocker;

            $result[] = [$paketshopId, $depot, $firma, $address, $zip, $ort];
        }

        return $result;
    }


    public function createMap(array $data)
    {
        $result = [];

        foreach ($data as $parcelNetworkItem) {

            list($paketshopId, $depot, $firma, $address, $zip, $ort) = $parcelNetworkItem;

            $result[$ort][] = [
                'name' => $firma,
                'address' => $address,
                'zip' => $zip,
                'place_id' => $paketshopId,
            ];
        }

        return $result;
    }
}