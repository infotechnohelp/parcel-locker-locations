<?php

require 'src/ParcelNetworkLocations.php';
require 'src/OmnivaParcelNetworkLocations.php';
require 'src/ItellaParcelNetworkLocations.php';
require 'src/DpdParcelNetworkLocations.php';

$parcelLockers = (new DpdParcelNetworkLocations())->extractByParameters();

echo count($parcelLockers) . "\n";

$map = (new DpdParcelNetworkLocations())->createMap($parcelLockers);

file_put_contents('Maps/dpd.php', "<?php\nreturn " . var_export($map, true) . ";");



$parcelLockers = (new OmnivaParcelNetworkLocations())->extractByParameters(0);

echo count($parcelLockers) . "\n";

$map = (new OmnivaParcelNetworkLocations())->createMap($parcelLockers);

file_put_contents('Maps/omniva.php', "<?php\nreturn " . var_export($map, true) . ";");



$parcelLockers = (new ItellaParcelNetworkLocations())->extractByParameters();

echo count($parcelLockers) . "\n";

$map = (new ItellaParcelNetworkLocations())->createMap($parcelLockers);

file_put_contents('Maps/itella.php', "<?php\nreturn " . var_export($map, true) . ";");