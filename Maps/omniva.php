<?php
return array (
  'Ida-Viru maakond' => 
  array (
    'Kohtla-Järve linn' => 
    array (
      'Ahtme linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Ahtme Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Maleva tänav',
            1 => '23',
          ),
          'zip' => '96142',
        ),
        1 => 
        array (
          'name' => 'Kohtla-Järve Ahtme Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Puru tee',
            1 => '77',
          ),
          'zip' => '96123',
        ),
      ),
      'Järve linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Kohtla-Järve Keskallee Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Keskallee',
            1 => '12',
          ),
          'zip' => '96139',
        ),
        1 => 
        array (
          'name' => 'Kohtla-Järve Mõisa tee Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Mõisa tee',
            1 => '3',
          ),
          'zip' => '96307',
        ),
        2 => 
        array (
          'name' => 'Kohtla-Järve Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Põhja allee',
            1 => '4',
          ),
          'zip' => '96036',
        ),
        3 => 
        array (
          'name' => 'Kohtla-Järve Vironia Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Järveküla tee',
            1 => '50',
          ),
          'zip' => '96093',
        ),
      ),
    ),
    'Alutaguse vald' => 
    array (
      'Iisaku alevik' => 
      array (
        0 => 
        array (
          'name' => 'Iisaku Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Tartu maantee',
            1 => '55',
          ),
          'zip' => '96189',
        ),
      ),
    ),
    'Jõhvi vald' => 
    array (
      'Jõhvi linn' => 
      array (
        0 => 
        array (
          'name' => 'Jõhvi Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Tartu maantee',
            1 => '15',
          ),
          'zip' => '96191',
        ),
        1 => 
        array (
          'name' => 'Jõhvi Jewe Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Narva maantee',
            1 => '8',
          ),
          'zip' => '96030',
        ),
        2 => 
        array (
          'name' => 'Jõhvi Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Rakvere tänav',
            1 => '29',
          ),
          'zip' => '96089',
        ),
      ),
    ),
    'Lüganuse vald' => 
    array (
      'Kiviõli linn' => 
      array (
        0 => 
        array (
          'name' => 'Kiviõli Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Keskpuiestee',
            1 => '33',
          ),
          'zip' => '96176',
        ),
        1 => 
        array (
          'name' => 'Kiviõli Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Viru tänav',
            1 => '5',
          ),
          'zip' => '96090',
        ),
      ),
    ),
    'Narva linn' => 
    array (
      0 => 
      array (
        'name' => 'Narva Astri keskuse pakiautomaat',
        'address' => 
        array (
          0 => 'Tallinna maantee',
          1 => '41',
        ),
        'zip' => '96028',
      ),
      1 => 
      array (
        'name' => 'Narva Fama Keskuse pakiautomaat',
        'address' => 
        array (
          0 => 'Fama tänav',
          1 => '10',
        ),
        'zip' => '96072',
      ),
      2 => 
      array (
        'name' => 'Narva Kerese keskuse pakiautomaat',
        'address' => 
        array (
          0 => 'Paul Kerese tänav',
          1 => '3',
        ),
        'zip' => '96218',
      ),
      3 => 
      array (
        'name' => 'Narva Kreenholmi Maxima XX pakiautomaat',
        'address' => 
        array (
          0 => 'Kreenholmi tänav',
          1 => '52',
        ),
        'zip' => '96091',
      ),
      4 => 
      array (
        'name' => 'Narva Megamarketi pakiautomaat',
        'address' => 
        array (
          0 => 'Rakvere tänav',
          1 => '71',
        ),
        'zip' => '96217',
      ),
      5 => 
      array (
        'name' => 'Narva Prisma pakiautomaat',
        'address' => 
        array (
          0 => 'Kangelaste prospekt',
          1 => '29',
        ),
        'zip' => '96125',
      ),
      6 => 
      array (
        'name' => 'Narva Tiimani Maxima XX pakiautomaat',
        'address' => 
        array (
          0 => 'Albert-August Tiimanni tänav',
          1 => '20',
        ),
        'zip' => '96182',
      ),
    ),
    'Narva-Jõesuu linn' => 
    array (
      'Narva-Jõesuu linn' => 
      array (
        0 => 
        array (
          'name' => 'Narva-Jõesuu raamatukogu pakiautomaat',
          'address' => 
          array (
            0 => 'Kesk tänav',
            1 => '3',
          ),
          'zip' => '96326',
        ),
      ),
    ),
    'Sillamäe linn' => 
    array (
      0 => 
      array (
        'name' => 'Sillamäe Maksimarketi pakiautomaat',
        'address' => 
        array (
          0 => 'Viru puiestee',
          1 => '5',
        ),
        'zip' => '96054',
      ),
      1 => 
      array (
        'name' => 'Sillamäe Viru pst Maxima pakiautomaat',
        'address' => 
        array (
          0 => 'Viru puiestee',
          1 => '35',
        ),
        'zip' => '96222',
      ),
    ),
  ),
  'Võru maakond' => 
  array (
    'Antsla vald' => 
    array (
      'Antsla linn' => 
      array (
        0 => 
        array (
          'name' => 'Antsla Konsum pakiautomaat',
          'address' => 
          array (
            0 => 'Põllu tänav',
            1 => '15',
          ),
          'zip' => '96118',
        ),
      ),
    ),
    'Võru linn' => 
    array (
      0 => 
      array (
        'name' => 'Võru Maksimarketi pakiautomaat',
        'address' => 
        array (
          0 => 'Jüri tänav',
          1 => '83',
        ),
        'zip' => '96035',
      ),
      1 => 
      array (
        'name' => 'Võru Maxima pakiautomaat',
        'address' => 
        array (
          0 => 'Kooli tänav',
          1 => '2',
        ),
        'zip' => '96064',
      ),
      2 => 
      array (
        'name' => 'Võru kesklinna Circle K pakiautomaat',
        'address' => 
        array (
          0 => 'Jüri tänav',
          1 => '45',
        ),
        'zip' => '96314',
      ),
    ),
  ),
  'Järva maakond' => 
  array (
    'Järva vald' => 
    array (
      'Aravete alevik' => 
      array (
        0 => 
        array (
          'name' => 'Aravete Meie kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Piibe maantee',
            1 => '16',
          ),
          'zip' => '96230',
        ),
      ),
      'Järva-Jaani alev' => 
      array (
        0 => 
        array (
          'name' => 'Järva-Jaani Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Lai tänav',
            1 => '23',
          ),
          'zip' => '96157',
        ),
      ),
      'Koeru alevik' => 
      array (
        0 => 
        array (
          'name' => 'Koeru Vallamaja pakiautomaat',
          'address' => 
          array (
            0 => 'Paide tee',
            1 => '5',
          ),
          'zip' => '96147',
        ),
      ),
    ),
    'Paide linn' => 
    array (
      'Paide linn' => 
      array (
        0 => 
        array (
          'name' => 'Paide Hesburgeri pakiautomaat',
          'address' => 
          array (
            0 => 'Raudtee tänav',
            1 => '29',
          ),
          'zip' => '96207',
        ),
        1 => 
        array (
          'name' => 'Paide Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Aiavilja tänav',
            1 => '4',
          ),
          'zip' => '96037',
        ),
      ),
    ),
    'Türi vald' => 
    array (
      'Türi linn' => 
      array (
        0 => 
        array (
          'name' => 'Türi Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Viljandi tänav',
            1 => '10',
          ),
          'zip' => '96052',
        ),
      ),
    ),
  ),
  'Harju maakond' => 
  array (
    'Raasiku vald' => 
    array (
      'Aruküla alevik' => 
      array (
        0 => 
        array (
          'name' => 'Aruküla Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '8',
          ),
          'zip' => '96204',
        ),
      ),
      'Raasiku alevik' => 
      array (
        0 => 
        array (
          'name' => 'Raasiku pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '19',
          ),
          'zip' => '96105',
        ),
      ),
    ),
    'Viimsi vald' => 
    array (
      'Haabneeme alevik' => 
      array (
        0 => 
        array (
          'name' => 'Haabneeme Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Rohuneeme tee',
            1 => '32',
          ),
          'zip' => '96206',
        ),
        1 => 
        array (
          'name' => 'Viimsi Kaubanduskeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Randvere tee',
            1 => '6',
          ),
          'zip' => '96013',
        ),
        2 => 
        array (
          'name' => 'Viimsi Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Randvere tee',
            1 => '9',
          ),
          'zip' => '96209',
        ),
      ),
      'Laiaküla' => 
      array (
        0 => 
        array (
          'name' => 'Viimsi Pärnamäe Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Lilleoru tee',
            1 => '4',
          ),
          'zip' => '96124',
        ),
      ),
    ),
    'Rae vald' => 
    array (
      'Jüri alevik' => 
      array (
        0 => 
        array (
          'name' => 'Jüri Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Aruküla tee',
            1 => '7',
          ),
          'zip' => '96188',
        ),
        1 => 
        array (
          'name' => 'Jüri Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Aruküla tee',
            1 => '29',
          ),
          'zip' => '96079',
        ),
      ),
      'Lagedi alevik' => 
      array (
        0 => 
        array (
          'name' => 'Lagedi Keskusehoone pakiautomaat',
          'address' => 
          array (
            0 => 'Kooli tänav',
            1 => '18b',
          ),
          'zip' => '96183',
        ),
      ),
      'Peetri alevik' => 
      array (
        0 => 
        array (
          'name' => 'Peetri Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Veesaare tee',
            1 => '2',
          ),
          'zip' => '96082',
        ),
      ),
    ),
    'Anija vald' => 
    array (
      'Kehra linn' => 
      array (
        0 => 
        array (
          'name' => 'Kehra pakiautomaat',
          'address' => 
          array (
            0 => 'Kose maantee',
            1 => '9',
          ),
          'zip' => '96114',
        ),
      ),
    ),
    'Keila linn' => 
    array (
      0 => 
      array (
        'name' => 'Keila Grossi pakiautomaat',
        'address' => 
        array (
          0 => 'Piiri tänav',
          1 => '7',
        ),
        'zip' => '96172',
      ),
      1 => 
      array (
        'name' => 'Keila Maxima pakiautomaat',
        'address' => 
        array (
          0 => 'Tallinna maantee',
          1 => '25',
        ),
        'zip' => '96229',
      ),
      2 => 
      array (
        'name' => 'Keila Rõõmu kaubamaja pakiautomaat',
        'address' => 
        array (
          0 => 'Haapsalu maantee',
          1 => '57a',
        ),
        'zip' => '96049',
      ),
    ),
    'Kiili vald' => 
    array (
      'Kiili alev' => 
      array (
        0 => 
        array (
          'name' => 'Kiili Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Nabala tee',
            1 => '4',
          ),
          'zip' => '96155',
        ),
      ),
    ),
    'Saku vald' => 
    array (
      'Kiisa alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kiisa A ja O pakiautomaat',
          'address' => 
          array (
            0 => 'Kurtna tee',
            1 => '10',
          ),
          'zip' => '96159',
        ),
      ),
      'Saku alevik' => 
      array (
        0 => 
        array (
          'name' => 'Saku Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Pargi tänav',
            1 => '2',
          ),
          'zip' => '96050',
        ),
        1 => 
        array (
          'name' => 'Saku Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Üksnurme tee',
            1 => '2',
          ),
          'zip' => '96301',
        ),
      ),
    ),
    'Kose vald' => 
    array (
      'Kose alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kose Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Kodu tänav',
            1 => '2',
          ),
          'zip' => '96096',
        ),
      ),
    ),
    'Jõelähtme vald' => 
    array (
      'Kostivere alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kostivere Meie kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Jõe tänav',
            1 => '3',
          ),
          'zip' => '96160',
        ),
      ),
      'Loo alevik' => 
      array (
        0 => 
        array (
          'name' => 'Loo Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Saha tee',
            1 => '9',
          ),
          'zip' => '96171',
        ),
      ),
    ),
    'Kuusalu vald' => 
    array (
      'Kuusalu alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kuusalu pakiautomaat',
          'address' => 
          array (
            0 => 'Kuusalu tee',
            1 => '13',
          ),
          'zip' => '96108',
        ),
      ),
    ),
    'Saue vald' => 
    array (
      'Laagri alevik' => 
      array (
        0 => 
        array (
          'name' => 'Laagri CoMarketi pakiautomaat',
          'address' => 
          array (
            0 => 'Veskitammi tänav',
            1 => '10',
          ),
          'zip' => '96200',
        ),
        1 => 
        array (
          'name' => 'Laagri Maksimarketi pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '558a',
          ),
          'zip' => '96067',
        ),
      ),
      'Saue linn' => 
      array (
        0 => 
        array (
          'name' => 'Saue Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnasalu põik',
            1 => '1b',
          ),
          'zip' => '96184',
        ),
        1 => 
        array (
          'name' => 'Saue Kaubakeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Ridva tänav',
            1 => '15',
          ),
          'zip' => '96055',
        ),
      ),
      'Turba alevik' => 
      array (
        0 => 
        array (
          'name' => 'Turba kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Tehase tänav',
            1 => '1',
          ),
          'zip' => '96223',
        ),
      ),
    ),
    'Loksa linn' => 
    array (
      0 => 
      array (
        'name' => 'Loksa Grossi pakiautomaat',
        'address' => 
        array (
          0 => 'Rohuaia tänav',
          1 => '6',
        ),
        'zip' => '96134',
      ),
    ),
    'Maardu linn' => 
    array (
      0 => 
      array (
        'name' => 'Maardu Maxima X pakiautomaat',
        'address' => 
        array (
          0 => 'Keemikute tänav',
          1 => '37',
        ),
        'zip' => '96300',
      ),
      1 => 
      array (
        'name' => 'Maardu Maxima XX pakiautomaat',
        'address' => 
        array (
          0 => 'Keemikute tänav',
          1 => '2',
        ),
        'zip' => '96081',
      ),
      2 => 
      array (
        'name' => 'Maardu Pärli Keskuse pakiautomaat',
        'address' => 
        array (
          0 => 'Nurga tänav',
          1 => '3',
        ),
        'zip' => '96130',
      ),
      3 => 
      array (
        'name' => 'Muuga Maxima pakiautomaat',
        'address' => 
        array (
          0 => 'Altmetsa tee',
          1 => '1',
        ),
        'zip' => '96228',
      ),
    ),
    'Lääne-Harju vald' => 
    array (
      'Paldiski linn' => 
      array (
        0 => 
        array (
          'name' => 'Paldiski Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Rae tänav',
            1 => '38',
          ),
          'zip' => '96203',
        ),
        1 => 
        array (
          'name' => 'Paldiski Maxima X pakiautomaat',
          'address' => 
          array (
            0 => 'Rae tänav',
            1 => '14',
          ),
          'zip' => '96097',
        ),
      ),
    ),
    'Harku vald' => 
    array (
      'Tabasalu alevik' => 
      array (
        0 => 
        array (
          'name' => 'Tabasalu Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Klooga maantee',
            1 => '10b',
          ),
          'zip' => '96071',
        ),
      ),
      'Vääna-Jõesuu küla' => 
      array (
        0 => 
        array (
          'name' => 'Vääna-Jõesuu kauplus Maksis pakiautomaat',
          'address' => 
          array (
            0 => '11390 Tallinn-Rannamõisa-Kloogaranna maantee',
          ),
          'zip' => '96325',
        ),
      ),
    ),
    'Tallinn' => 
    array (
      'Mustamäe linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Akadeemia Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Akadeemia tee',
            1 => '35',
          ),
          'zip' => '96003',
        ),
        1 => 
        array (
          'name' => 'Tallinna Ehitajate tee Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Ehitajate tee',
            1 => '41',
          ),
          'zip' => '96216',
        ),
        2 => 
        array (
          'name' => 'Tallinna Kadaka Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Kadaka tee',
            1 => '56a',
          ),
          'zip' => '96018',
        ),
        3 => 
        array (
          'name' => 'Tallinna Magistrali Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Sõpruse puiestee',
            1 => '201',
          ),
          'zip' => '96056',
        ),
        4 => 
        array (
          'name' => 'Tallinna Mustamäe Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'A. H. Tammsaare tee',
            1 => '104a',
          ),
          'zip' => '96141',
        ),
        5 => 
        array (
          'name' => 'Tallinna Mustika Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Karjavälja tänav',
            1 => '4',
          ),
          'zip' => '96115',
        ),
        6 => 
        array (
          'name' => 'Tallinna Sõpruse pst 171 Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Sõpruse puiestee',
            1 => '171',
          ),
          'zip' => '96306',
        ),
        7 => 
        array (
          'name' => 'Tallinna Sütiste Maxima X pakiautomaat',
          'address' => 
          array (
            0 => 'Juhan Sütiste tee',
            1 => '28',
          ),
          'zip' => '96175',
        ),
        8 => 
        array (
          'name' => 'Tallinna Tammsaare Maxima XX pakiautomaat',
          'address' => 
          array (
            0 => 'A. H. Tammsaare tee',
            1 => '133',
          ),
          'zip' => '96305',
        ),
        9 => 
        array (
          'name' => 'Tallinna Vilde tee Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Eduard Vilde tee',
            1 => '75',
          ),
          'zip' => '96143',
        ),
      ),
      'Põhja-Tallinna linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Arsenali keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Erika tänav',
            1 => '14',
          ),
          'zip' => '96133',
        ),
        1 => 
        array (
          'name' => 'Tallinna Balti Jaama pakiautomaat',
          'address' => 
          array (
            0 => 'Toompuiestee',
            1 => '37',
          ),
          'zip' => '96001',
        ),
        2 => 
        array (
          'name' => 'Tallinna Kolde Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Sõle tänav',
            1 => '27',
          ),
          'zip' => '96069',
        ),
        3 => 
        array (
          'name' => 'Tallinna Merimetsa Selveri pakiautomaa',
          'address' => 
          array (
            0 => 'Paldiski maantee',
            1 => '56',
          ),
          'zip' => '96129',
        ),
        4 => 
        array (
          'name' => 'Tallinna Pelgulinna Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Sõle tänav',
            1 => '51',
          ),
          'zip' => '96017',
        ),
        5 => 
        array (
          'name' => 'Tallinna Põhja Neste pakiautomaat',
          'address' => 
          array (
            0 => 'Soo tänav',
            1 => '1',
          ),
          'zip' => '96181',
        ),
        6 => 
        array (
          'name' => 'Tallinna Stroomi Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Tuulemaa tänav',
            1 => '20',
          ),
          'zip' => '96154',
        ),
        7 => 
        array (
          'name' => 'Tallinna Sõle Rimi (Paavli 10) pakiautomaat',
          'address' => 
          array (
            0 => 'Paavli tänav',
            1 => '10',
          ),
          'zip' => '96166',
        ),
        8 => 
        array (
          'name' => 'Tallinna Telliskivi Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Telliskivi tänav',
            1 => '61',
          ),
          'zip' => '96058',
        ),
        9 => 
        array (
          'name' => 'Tallinna Tööstuse Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Tööstuse tänav',
            1 => '101',
          ),
          'zip' => '96185',
        ),
      ),
      'Kesklinna linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Coca-Cola Plaza pakiautomaat',
          'address' => 
          array (
            0 => 'Hobujaama tänav',
            1 => '5',
          ),
          'zip' => '96219',
        ),
        1 => 
        array (
          'name' => 'Tallinna Juhkentali Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Juhkentali tänav',
            1 => '35',
          ),
          'zip' => '96195',
        ),
        2 => 
        array (
          'name' => 'Tallinna Lastekodu Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Lastekodu tänav',
            1 => '14',
          ),
          'zip' => '96180',
        ),
        3 => 
        array (
          'name' => 'Tallinna Magdaleena pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '106',
          ),
          'zip' => '96086',
        ),
        4 => 
        array (
          'name' => 'Tallinna Nautica Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Ahtri tänav',
            1 => '9',
          ),
          'zip' => '96174',
        ),
        5 => 
        array (
          'name' => 'Tallinna Solaris Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Estonia puiestee',
            1 => '9',
          ),
          'zip' => '96140',
        ),
        6 => 
        array (
          'name' => 'Tallinna Stockmanni pakiautomaat',
          'address' => 
          array (
            0 => 'Liivalaia tänav',
            1 => '53',
          ),
          'zip' => '96057',
        ),
        7 => 
        array (
          'name' => 'Tallinna Tondi Ärikeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '142',
          ),
          'zip' => '96153',
        ),
        8 => 
        array (
          'name' => 'Tallinna Torupilli Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Vesivärava tänav',
            1 => '37',
          ),
          'zip' => '96038',
        ),
        9 => 
        array (
          'name' => 'Tallinna Viru Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Viru väljak',
            1 => '4',
          ),
          'zip' => '96002',
        ),
        10 => 
        array (
          'name' => 'Tallinna Viru bussiterminali pakiautomaat',
          'address' => 
          array (
            0 => 'Viru väljak',
            1 => '4',
          ),
          'zip' => '96104',
        ),
      ),
      'Haabersti linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Ehitajate tee Maxima XXX pakiautomaat',
          'address' => 
          array (
            0 => 'Ehitajate tee',
            1 => '148',
          ),
          'zip' => '96303',
        ),
        1 => 
        array (
          'name' => 'Tallinna Haabersti RIMI pakiautomaat',
          'address' => 
          array (
            0 => 'Haabersti tänav',
            1 => '1',
          ),
          'zip' => '96111',
        ),
        2 => 
        array (
          'name' => 'Tallinna Järveotsa Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Järveotsa tee',
            1 => '35b',
          ),
          'zip' => '96138',
        ),
        3 => 
        array (
          'name' => 'Tallinna Kakumäe Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Rannamõisa tee',
            1 => '6',
          ),
          'zip' => '96015',
        ),
        4 => 
        array (
          'name' => 'Tallinna Kollase keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Õismäe tee',
            1 => '107',
          ),
          'zip' => '96226',
        ),
        5 => 
        array (
          'name' => 'Tallinna Nurmenuku pakiautomaat',
          'address' => 
          array (
            0 => 'Õismäe tee',
            1 => '1b',
          ),
          'zip' => '96004',
        ),
        6 => 
        array (
          'name' => 'Tallinna Rocca al Mare pakiautomaat',
          'address' => 
          array (
            0 => 'Paldiski maantee',
            1 => '102',
          ),
          'zip' => '96005',
        ),
        7 => 
        array (
          'name' => 'Tallinna Õismäe Maxima XX pakiautomaat',
          'address' => 
          array (
            0 => 'Õismäe tee',
            1 => '46',
          ),
          'zip' => '96103',
        ),
      ),
      'Lasnamäe linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Idakeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Punane tänav',
            1 => '16',
          ),
          'zip' => '96202',
        ),
        1 => 
        array (
          'name' => 'Tallinna Kivila Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Kivila tänav',
            1 => '26',
          ),
          'zip' => '96190',
        ),
        2 => 
        array (
          'name' => 'Tallinna Kärberi Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Kristjan Kärberi tänav',
            1 => '20a',
          ),
          'zip' => '96162',
        ),
        3 => 
        array (
          'name' => 'Tallinna Lasnamäe Centrumi A pakiautomaat',
          'address' => 
          array (
            0 => 'Mustakivi tee',
            1 => '13',
          ),
          'zip' => '96135',
        ),
        4 => 
        array (
          'name' => 'Tallinna Lasnamäe Centrumi B pakiautomaat',
          'address' => 
          array (
            0 => 'Mustakivi tee',
            1 => '13',
          ),
          'zip' => '96041',
        ),
        5 => 
        array (
          'name' => 'Tallinna Lasnamäe Prisma pakiautomaat',
          'address' => 
          array (
            0 => 'Mustakivi tee',
            1 => '17',
          ),
          'zip' => '96066',
        ),
        6 => 
        array (
          'name' => 'Tallinna Lauluväljaku Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Juhan Smuuli tee',
            1 => '9',
          ),
          'zip' => '96080',
        ),
        7 => 
        array (
          'name' => 'Tallinna Linnamäe Maxima XXX pakiautomaat',
          'address' => 
          array (
            0 => 'Linnamäe tee',
            1 => '57',
          ),
          'zip' => '96116',
        ),
        8 => 
        array (
          'name' => 'Tallinna Läänemere Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Läänemere tee',
            1 => '2c',
          ),
          'zip' => '96194',
        ),
        9 => 
        array (
          'name' => 'Tallinna Läänemere Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Läänemere tee',
            1 => '28',
          ),
          'zip' => '96173',
        ),
        10 => 
        array (
          'name' => 'Tallinna Mustakivi Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Mahtra tänav',
            1 => '1',
          ),
          'zip' => '96010',
        ),
        11 => 
        array (
          'name' => 'Tallinna Paasiku Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Paasiku tänav',
            1 => '2a',
          ),
          'zip' => '96193',
        ),
        12 => 
        array (
          'name' => 'Tallinna Paepargi Maxima XX pakiautomaat',
          'address' => 
          array (
            0 => 'Paepargi tänav',
            1 => '57',
          ),
          'zip' => '96131',
        ),
        13 => 
        array (
          'name' => 'Tallinna Pallasti pakiautomaat',
          'address' => 
          array (
            0 => 'Pallasti tänav',
            1 => '28',
          ),
          'zip' => '96146',
        ),
        14 => 
        array (
          'name' => 'Tallinna Peterburi tee 34/4 pakiautomaat',
          'address' => 
          array (
            0 => 'Peterburi tee',
            1 => '34',
          ),
          'zip' => '96317',
        ),
        15 => 
        array (
          'name' => 'Tallinna Punase Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Punane tänav',
            1 => '46',
          ),
          'zip' => '96083',
        ),
        16 => 
        array (
          'name' => 'Tallinna Sikupilli pakiautomaat',
          'address' => 
          array (
            0 => 'Tartu maantee',
            1 => '87',
          ),
          'zip' => '96009',
        ),
        17 => 
        array (
          'name' => 'Tallinna T1 Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Peterburi tee',
            1 => '2',
          ),
          'zip' => '96323',
        ),
        18 => 
        array (
          'name' => 'Tallinna Tähesaju Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Tähesaju tee',
            1 => '1',
          ),
          'zip' => '96302',
        ),
        19 => 
        array (
          'name' => 'Tallinna Vikerlase Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Vikerlase tänav',
            1 => '19',
          ),
          'zip' => '96215',
        ),
        20 => 
        array (
          'name' => 'Tallinna Ülemiste City Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Sepapaja tänav',
            1 => '2',
          ),
          'zip' => '96208',
        ),
        21 => 
        array (
          'name' => 'Tallinna Ülemiste Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Suur-Sõjamäe tänav',
            1 => '4',
          ),
          'zip' => '96113',
        ),
        22 => 
        array (
          'name' => 'Tallinna Ümera Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Laagna tee',
            1 => '80',
          ),
          'zip' => '96059',
        ),
      ),
      'Nõmme linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Järve Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '238',
          ),
          'zip' => '96016',
        ),
        1 => 
        array (
          'name' => 'Tallinna Nõmme turu pakiautomaat',
          'address' => 
          array (
            0 => 'Turu plats',
            1 => '8',
          ),
          'zip' => '96101',
        ),
        2 => 
        array (
          'name' => 'Tallinna Pärnu mnt 390b Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '390',
          ),
          'zip' => '96304',
        ),
        3 => 
        array (
          'name' => 'Tallinna Pääsküla RIMI pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '453e',
          ),
          'zip' => '96008',
        ),
        4 => 
        array (
          'name' => 'Tallinna Raudalu Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Viljandi maantee',
            1 => '41a',
          ),
          'zip' => '96084',
        ),
        5 => 
        array (
          'name' => 'Tallinna Valdeku CoMarketi pakiautomaat',
          'address' => 
          array (
            0 => 'Vabaduse puiestee',
            1 => '54b',
          ),
          'zip' => '96132',
        ),
        6 => 
        array (
          'name' => 'Tallinna Valdeku Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Valdeku tänav',
            1 => '114',
          ),
          'zip' => '96145',
        ),
      ),
      'Kristiine linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Kristiine Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Endla tänav',
            1 => '45',
          ),
          'zip' => '96007',
        ),
        1 => 
        array (
          'name' => 'Tallinna Marienthali Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Mustamäe tee',
            1 => '16',
          ),
          'zip' => '96098',
        ),
        2 => 
        array (
          'name' => 'Tallinna Nõmme tee 23 Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Nõmme tee',
            1 => '23',
          ),
          'zip' => '96327',
        ),
        3 => 
        array (
          'name' => 'Tallinna Sõpruse Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Sõpruse puiestee',
            1 => '174',
          ),
          'zip' => '96014',
        ),
        4 => 
        array (
          'name' => 'Tallinna Tammsaare Ärikeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'A. H. Tammsaare tee',
            1 => '47',
          ),
          'zip' => '96214',
        ),
        5 => 
        array (
          'name' => 'Tallinna Tondi Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'A. H. Tammsaare tee',
            1 => '62',
          ),
          'zip' => '96006',
        ),
      ),
      'Pirita linnaosa' => 
      array (
        0 => 
        array (
          'name' => 'Tallinna Pirita Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Merivälja tee',
            1 => '24',
          ),
          'zip' => '96205',
        ),
        1 => 
        array (
          'name' => 'Tallinna Pirita Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Rummu tee',
            1 => '4',
          ),
          'zip' => '96012',
        ),
      ),
    ),
  ),
  'Lääne-Viru maakond' => 
  array (
    'Viru-Nigula vald' => 
    array (
      'Aseri alevik' => 
      array (
        0 => 
        array (
          'name' => 'Aseri Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Tehase tänav',
            1 => '23',
          ),
          'zip' => '96211',
        ),
      ),
      'Kunda linn' => 
      array (
        0 => 
        array (
          'name' => 'Kunda Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Mäe tänav',
            1 => '33',
          ),
          'zip' => '96122',
        ),
      ),
    ),
    'Haljala vald' => 
    array (
      'Haljala alevik' => 
      array (
        0 => 
        array (
          'name' => 'Haljala Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Võsu maantee',
            1 => '5',
          ),
          'zip' => '96324',
        ),
      ),
    ),
    'Kadrina vald' => 
    array (
      'Kadrina alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kadrina Konsum pakiautomaat',
          'address' => 
          array (
            0 => 'Viitna tee',
            1 => '4',
          ),
          'zip' => '96117',
        ),
      ),
    ),
    'Rakvere vald' => 
    array (
      'Tõrremäe küla' => 
      array (
        0 => 
        array (
          'name' => 'Rakvere Põhjakeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Haljala tee',
            1 => '4',
          ),
          'zip' => '96078',
        ),
      ),
    ),
    'Rakvere linn' => 
    array (
      0 => 
      array (
        'name' => 'Rakvere Turutare pakiautomaat',
        'address' => 
        array (
          0 => 'Laada tänav',
          1 => '18',
        ),
        'zip' => '96031',
      ),
      1 => 
      array (
        'name' => 'Rakvere Vaala Keskuse pakiautomaat',
        'address' => 
        array (
          0 => 'Lõõtspilli tänav',
          1 => '2',
        ),
        'zip' => '96167',
      ),
    ),
    'Tapa vald' => 
    array (
      'Tamsalu linn' => 
      array (
        0 => 
        array (
          'name' => 'Tamsalu Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Tehnika tänav',
            1 => '24',
          ),
          'zip' => '96120',
        ),
      ),
      'Tapa linn' => 
      array (
        0 => 
        array (
          'name' => 'Tapa Grossi pakiautomaat',
          'address' => 
          array (
            0 => 'Jaama tänav',
            1 => '1',
          ),
          'zip' => '96092',
        ),
        1 => 
        array (
          'name' => 'Tapa Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Pikk tänav',
            1 => '33',
          ),
          'zip' => '96308',
        ),
      ),
    ),
    'Vinni vald' => 
    array (
      'Vinni alevik' => 
      array (
        0 => 
        array (
          'name' => 'Vinni Spordikompleksi pakiautomaat',
          'address' => 
          array (
            0 => 'Sõpruse tänav',
            1 => '16',
          ),
          'zip' => '96149',
        ),
      ),
    ),
    'Väike-Maarja vald' => 
    array (
      'Väike-Maarja alevik' => 
      array (
        0 => 
        array (
          'name' => 'Väike-Maarja pakiautomaat',
          'address' => 
          array (
            0 => 'Pikk tänav',
            1 => '9',
          ),
          'zip' => '96107',
        ),
      ),
    ),
  ),
  'Tartu maakond' => 
  array (
    'Elva vald' => 
    array (
      'Elva linn' => 
      array (
        0 => 
        array (
          'name' => 'Elva Arbimäe Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Kirde tänav',
            1 => '3',
          ),
          'zip' => '96179',
        ),
        1 => 
        array (
          'name' => 'Elva Turuplatsi Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Kesk tänav',
            1 => '1',
          ),
          'zip' => '96047',
        ),
      ),
      'Puhja alevik' => 
      array (
        0 => 
        array (
          'name' => 'Puhja Coop kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Nooruse tänav',
            1 => '2',
          ),
          'zip' => '96150',
        ),
      ),
      'Rõngu alevik' => 
      array (
        0 => 
        array (
          'name' => 'Rõngu Coop kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Tartu maantee',
            1 => '2',
          ),
          'zip' => '96151',
        ),
      ),
    ),
    'Luunja vald' => 
    array (
      'Veibri küla' => 
      array (
        0 => 
        array (
          'name' => 'Ihaste Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Lillevälja tee',
            1 => '1',
          ),
          'zip' => '96170',
        ),
      ),
    ),
    'Tartu vald' => 
    array (
      'Kõrveküla alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kõrveküla A ja O pakiautomaat',
          'address' => 
          array (
            0 => 'Vasula tee',
            1 => '4',
          ),
          'zip' => '96169',
        ),
      ),
      'Tabivere alevik' => 
      array (
        0 => 
        array (
          'name' => 'Tabivere Saadjärve A ja O pakiautomaat',
          'address' => 
          array (
            0 => 'Kalda tänav',
            1 => '1',
          ),
          'zip' => '96212',
        ),
      ),
    ),
    'Nõo vald' => 
    array (
      'Nõo alevik' => 
      array (
        0 => 
        array (
          'name' => 'Nõo vallamaja pakiautomaat',
          'address' => 
          array (
            0 => 'Voika tänav',
            1 => '23',
          ),
          'zip' => '96152',
        ),
      ),
    ),
    'Tartu linn' => 
    array (
      'Tartu linn' => 
      array (
        0 => 
        array (
          'name' => 'Tartu Aardla Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Võru tänav',
            1 => '75b',
          ),
          'zip' => '96310',
        ),
        1 => 
        array (
          'name' => 'Tartu Anne 40 Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Anne tänav',
            1 => '40',
          ),
          'zip' => '96319',
        ),
        2 => 
        array (
          'name' => 'Tartu Anne Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Kalda tee',
            1 => '43',
          ),
          'zip' => '96126',
        ),
        3 => 
        array (
          'name' => 'Tartu Eedeni pakiautomaat',
          'address' => 
          array (
            0 => 'Kalda tee',
            1 => '1c',
          ),
          'zip' => '96020',
        ),
        4 => 
        array (
          'name' => 'Tartu Jaamamõisa Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Jaama tänav',
            1 => '74',
          ),
          'zip' => '96213',
        ),
        5 => 
        array (
          'name' => 'Tartu Karete Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Jalaka tänav',
            1 => '34',
          ),
          'zip' => '96187',
        ),
        6 => 
        array (
          'name' => 'Tartu Kesklinna Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Küüni tänav',
            1 => '7',
          ),
          'zip' => '96061',
        ),
        7 => 
        array (
          'name' => 'Tartu Kivilinna pakiautomaat',
          'address' => 
          array (
            0 => 'Jaama tänav',
            1 => '173',
          ),
          'zip' => '96068',
        ),
        8 => 
        array (
          'name' => 'Tartu Kvartali Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Riia tänav',
            1 => '2',
          ),
          'zip' => '96165',
        ),
        9 => 
        array (
          'name' => 'Tartu Lembitu Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Lembitu tänav',
            1 => '2',
          ),
          'zip' => '96074',
        ),
        10 => 
        array (
          'name' => 'Tartu Lõunakeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Ringtee tänav',
            1 => '75',
          ),
          'zip' => '96019',
        ),
        11 => 
        array (
          'name' => 'Tartu Raadi Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Narva maantee',
            1 => '112',
          ),
          'zip' => '96127',
        ),
        12 => 
        array (
          'name' => 'Tartu Rebase Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Rebase tänav',
            1 => '10',
          ),
          'zip' => '96087',
        ),
        13 => 
        array (
          'name' => 'Tartu Ringtee Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Aardla tänav',
            1 => '114',
          ),
          'zip' => '96088',
        ),
        14 => 
        array (
          'name' => 'Tartu Saare Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Anne tänav',
            1 => '57',
          ),
          'zip' => '96136',
        ),
        15 => 
        array (
          'name' => 'Tartu Sõbra Prisma pakiautomaat',
          'address' => 
          array (
            0 => 'Sõbra tänav',
            1 => '56',
          ),
          'zip' => '96311',
        ),
        16 => 
        array (
          'name' => 'Tartu Sõbrakeskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Võru tänav',
            1 => '55f',
          ),
          'zip' => '96021',
        ),
        17 => 
        array (
          'name' => 'Tartu Tasku Keskuse pakiautomaat',
          'address' => 
          array (
            0 => 'Turu tänav',
            1 => '2',
          ),
          'zip' => '96039',
        ),
        18 => 
        array (
          'name' => 'Tartu Tuglase Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Friedebert Tuglase tänav',
            1 => '19',
          ),
          'zip' => '96321',
        ),
        19 => 
        array (
          'name' => 'Tartu Ujula Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Ujula tänav',
            1 => '2',
          ),
          'zip' => '96022',
        ),
        20 => 
        array (
          'name' => 'Tartu Vahi Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Vahi tänav',
            1 => '62',
          ),
          'zip' => '96309',
        ),
        21 => 
        array (
          'name' => 'Tartu Veeriku Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Vitamiini tänav',
            1 => '1',
          ),
          'zip' => '96060',
        ),
        22 => 
        array (
          'name' => 'Tartu Zeppelini pakiautomaat',
          'address' => 
          array (
            0 => 'Turu tänav',
            1 => '14',
          ),
          'zip' => '96225',
        ),
      ),
    ),
    'Kambja vald' => 
    array (
      'Tõrvandi alevik' => 
      array (
        0 => 
        array (
          'name' => 'Tõrvandi Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Müürsepa tänav',
            1 => '1',
          ),
          'zip' => '96178',
        ),
      ),
      'Ülenurme alevik' => 
      array (
        0 => 
        array (
          'name' => 'Ülenurme Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Võru maantee',
            1 => '2',
          ),
          'zip' => '96177',
        ),
      ),
    ),
  ),
  'Lääne maakond' => 
  array (
    'Haapsalu linn' => 
    array (
      'Haapsalu linn' => 
      array (
        0 => 
        array (
          'name' => 'Haapsalu Kastani pakiautomaat',
          'address' => 
          array (
            0 => 'Kuuse tänav',
            1 => '28',
          ),
          'zip' => '96128',
        ),
        1 => 
        array (
          'name' => 'Haapsalu Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '1',
          ),
          'zip' => '96033',
        ),
      ),
      'Uuemõisa alevik' => 
      array (
        0 => 
        array (
          'name' => 'Haapsalu Uuemõisa Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '84',
          ),
          'zip' => '96164',
        ),
      ),
    ),
    'Lääne-Nigula vald' => 
    array (
      'Risti alevik' => 
      array (
        0 => 
        array (
          'name' => 'Risti vallamaja pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '4',
          ),
          'zip' => '96196',
        ),
      ),
    ),
  ),
  'Rapla maakond' => 
  array (
    'Kehtna vald' => 
    array (
      'Järvakandi alev' => 
      array (
        0 => 
        array (
          'name' => 'Järvakandi Kandi Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '34',
          ),
          'zip' => '96224',
        ),
      ),
      'Kehtna alevik' => 
      array (
        0 => 
        array (
          'name' => 'Kehtna A ja O pakiautomaat',
          'address' => 
          array (
            0 => 'Lasteaia tänav',
            1 => '12',
          ),
          'zip' => '96221',
        ),
      ),
    ),
    'Kohila vald' => 
    array (
      'Kohila alev' => 
      array (
        0 => 
        array (
          'name' => 'Kohila Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Vabaduse tänav',
            1 => '7',
          ),
          'zip' => '96094',
        ),
      ),
    ),
    'Märjamaa vald' => 
    array (
      'Märjamaa alev' => 
      array (
        0 => 
        array (
          'name' => 'Märjamaa Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Kasti tee',
            1 => '1',
          ),
          'zip' => '96051',
        ),
      ),
    ),
    'Rapla vald' => 
    array (
      'Rapla linn' => 
      array (
        0 => 
        array (
          'name' => 'Rapla Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '50a',
          ),
          'zip' => '96043',
        ),
        1 => 
        array (
          'name' => 'Rapla Vestis Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '16',
          ),
          'zip' => '96220',
        ),
      ),
    ),
  ),
  'Jõgeva maakond' => 
  array (
    'Jõgeva vald' => 
    array (
      'Jõgeva linn' => 
      array (
        0 => 
        array (
          'name' => 'Jõgeva Postkontori pakiautomaat',
          'address' => 
          array (
            0 => 'Aia tänav',
            1 => '8',
          ),
          'zip' => '96044',
        ),
      ),
      'Torma alevik' => 
      array (
        0 => 
        array (
          'name' => 'Torma A ja O pakiautomaat',
          'address' => 
          array (
            0 => 'Kesk tänav',
            1 => '1a',
          ),
          'zip' => '96121',
        ),
      ),
    ),
    'Mustvee vald' => 
    array (
      'Mustvee linn' => 
      array (
        0 => 
        array (
          'name' => 'Mustvee Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Tartu tänav',
            1 => '3',
          ),
          'zip' => '96163',
        ),
      ),
    ),
    'Põltsamaa vald' => 
    array (
      'Põltsamaa linn' => 
      array (
        0 => 
        array (
          'name' => 'Põltsamaa Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Jõgeva maantee',
            1 => '1a',
          ),
          'zip' => '96048',
        ),
      ),
    ),
  ),
  'Viljandi maakond' => 
  array (
    'Mulgi vald' => 
    array (
      'Karksi-Nuia linn' => 
      array (
        0 => 
        array (
          'name' => 'Karksi-Nuia pakiautomaat',
          'address' => 
          array (
            0 => 'Rahumäe tänav',
            1 => '1',
          ),
          'zip' => '96110',
        ),
      ),
    ),
    'Viljandi linn' => 
    array (
      0 => 
      array (
        'name' => 'Viljandi Bussijaama pakiautomaat',
        'address' => 
        array (
          0 => 'Ilmarise tänav',
          1 => '1',
        ),
        'zip' => '96032',
      ),
      1 => 
      array (
        'name' => 'Viljandi Maksimarketi pakiautomaat',
        'address' => 
        array (
          0 => 'Lääne tänav',
          1 => '2',
        ),
        'zip' => '96070',
      ),
      2 => 
      array (
        'name' => 'Viljandi Männimäe Selveri pakiautomaat',
        'address' => 
        array (
          0 => 'Riia maantee',
          1 => '35',
        ),
        'zip' => '96063',
      ),
      3 => 
      array (
        'name' => 'Viljandi Tallinna 60 Maxima pakiautomaat',
        'address' => 
        array (
          0 => 'Tallinna tänav',
          1 => '60',
        ),
        'zip' => '96313',
      ),
      4 => 
      array (
        'name' => 'Viljandi Vaksali Maxima X pakiautomaat',
        'address' => 
        array (
          0 => 'Vaksali tänav',
          1 => '11b',
        ),
        'zip' => '96312',
      ),
    ),
  ),
  'Pärnu maakond' => 
  array (
    'Saarde vald' => 
    array (
      'Kilingi-Nõmme linn' => 
      array (
        0 => 
        array (
          'name' => 'Kilingi-Nõmme Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu tänav',
            1 => '37',
          ),
          'zip' => '96320',
        ),
      ),
    ),
    'Lääneranna vald' => 
    array (
      'Lihula linn' => 
      array (
        0 => 
        array (
          'name' => 'Lihula Konsum pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '12',
          ),
          'zip' => '96119',
        ),
      ),
    ),
    'Pärnu linn' => 
    array (
      'Paikuse alev' => 
      array (
        0 => 
        array (
          'name' => 'Paikuse Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnade puiestee',
            1 => '1',
          ),
          'zip' => '96158',
        ),
      ),
      'Pärnu linn' => 
      array (
        0 => 
        array (
          'name' => 'Pärnu Jannseni Rimi pakiautomaat',
          'address' => 
          array (
            0 => 'Johann Voldemar Jannseni tänav',
            1 => '3',
          ),
          'zip' => '96026',
        ),
        1 => 
        array (
          'name' => 'Pärnu Kaubamajaka pakiautomaat',
          'address' => 
          array (
            0 => 'Papiniidu tänav',
            1 => '8',
          ),
          'zip' => '96040',
        ),
        2 => 
        array (
          'name' => 'Pärnu Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Riia maantee',
            1 => '131',
          ),
          'zip' => '96027',
        ),
        3 => 
        array (
          'name' => 'Pärnu Oja Comarketi pakiautomaat',
          'address' => 
          array (
            0 => 'Liblika tänav',
            1 => '6',
          ),
          'zip' => '96315',
        ),
        4 => 
        array (
          'name' => 'Pärnu Port Artur 2 pakiautomaat',
          'address' => 
          array (
            0 => 'Lai tänav',
            1 => '11',
          ),
          'zip' => '96062',
        ),
        5 => 
        array (
          'name' => 'Pärnu Tiina Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Riia maantee',
            1 => '74',
          ),
          'zip' => '96197',
        ),
        6 => 
        array (
          'name' => 'Pärnu Turu pakiautomaat',
          'address' => 
          array (
            0 => 'Suur-Sepa tänav',
            1 => '18',
          ),
          'zip' => '96198',
        ),
        7 => 
        array (
          'name' => 'Pärnu Ülejõe Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna maantee',
            1 => '93a',
          ),
          'zip' => '96077',
        ),
      ),
      'Papsaare küla' => 
      array (
        0 => 
        array (
          'name' => 'Pärnu Maksimarketi pakiautomaat',
          'address' => 
          array (
            0 => 'Haapsalu maantee',
            1 => '43',
          ),
          'zip' => '96076',
        ),
      ),
    ),
    'Põhja-Pärnumaa vald' => 
    array (
      'Pärnu-Jaagupi alev' => 
      array (
        0 => 
        array (
          'name' => 'Pärnu-Jaagupi pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '8',
          ),
          'zip' => '96109',
        ),
      ),
      'Vändra alev' => 
      array (
        0 => 
        array (
          'name' => 'Vändra Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu-Paide maantee',
            1 => '21',
          ),
          'zip' => '96168',
        ),
      ),
    ),
    'Tori vald' => 
    array (
      'Sauga alevik' => 
      array (
        0 => 
        array (
          'name' => 'Sauga Coop kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Kuldnoka tänav',
            1 => '1',
          ),
          'zip' => '96148',
        ),
      ),
      'Sindi linn' => 
      array (
        0 => 
        array (
          'name' => 'Sindi Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Pärnu maantee',
            1 => '57',
          ),
          'zip' => '96316',
        ),
      ),
    ),
  ),
  'Saare maakond' => 
  array (
    'Saaremaa vald' => 
    array (
      'Kuressaare linn' => 
      array (
        0 => 
        array (
          'name' => 'Kuressaare Maxima XX pakiautomaat',
          'address' => 
          array (
            0 => 'Tallinna tänav',
            1 => '64',
          ),
          'zip' => '96034',
        ),
        1 => 
        array (
          'name' => 'Kuressaare Smuuli pakiautomaat',
          'address' => 
          array (
            0 => 'Juhan Smuuli tänav',
            1 => '1',
          ),
          'zip' => '96065',
        ),
        2 => 
        array (
          'name' => 'Saaremaa Kaubamaja pakiautomaat',
          'address' => 
          array (
            0 => 'Raekoja tänav',
            1 => '1',
          ),
          'zip' => '96199',
        ),
      ),
      'Orissaare alevik' => 
      array (
        0 => 
        array (
          'name' => 'Orissaare pakiautomaat',
          'address' => 
          array (
            0 => 'Kuivastu maantee',
            1 => '28',
          ),
          'zip' => '96106',
        ),
      ),
      'Salme alevik' => 
      array (
        0 => 
        array (
          'name' => 'Salme Rannapargi tee pakiautomaat',
          'address' => 
          array (
            0 => 'Rannapargi tee',
            1 => '4',
          ),
          'zip' => '96318',
        ),
      ),
    ),
    'Muhu vald' => 
    array (
      'Liiva küla' => 
      array (
        0 => 
        array (
          'name' => 'Muhu Liiva Alexela pakiautomaat',
          'address' => 
          array (
            0 => 'Liiva tankla',
          ),
          'zip' => '96161',
        ),
      ),
    ),
  ),
  'Hiiu maakond' => 
  array (
    'Hiiumaa vald' => 
    array (
      'Käina alevik' => 
      array (
        0 => 
        array (
          'name' => 'Käina Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Hiiu maantee',
            1 => '9',
          ),
          'zip' => '96156',
        ),
      ),
      'Kärdla linn' => 
      array (
        0 => 
        array (
          'name' => 'Kärdla Hiiu Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Keskväljak',
            1 => '1',
          ),
          'zip' => '96046',
        ),
        1 => 
        array (
          'name' => 'Kärdla Tormi Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Heltermaa maantee',
            1 => '14a',
          ),
          'zip' => '96210',
        ),
      ),
    ),
  ),
  'Valga maakond' => 
  array (
    'Otepää vald' => 
    array (
      'Otepää linn' => 
      array (
        0 => 
        array (
          'name' => 'Otepää Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Valga maantee',
            1 => '1b',
          ),
          'zip' => '96137',
        ),
      ),
    ),
    'Tõrva vald' => 
    array (
      'Tõrva linn' => 
      array (
        0 => 
        array (
          'name' => 'Tõrva Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Tamme tänav',
            1 => '2',
          ),
          'zip' => '96053',
        ),
      ),
    ),
    'Valga vald' => 
    array (
      'Valga linn' => 
      array (
        0 => 
        array (
          'name' => 'Valga Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Jaama puiestee',
            1 => '2b',
          ),
          'zip' => '96042',
        ),
        1 => 
        array (
          'name' => 'Valga Siili Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Alfred Neulandi tänav',
            1 => '2',
          ),
          'zip' => '96227',
        ),
      ),
    ),
  ),
  'Põlva maakond' => 
  array (
    'Põlva vald' => 
    array (
      'Põlva linn' => 
      array (
        0 => 
        array (
          'name' => 'Põlva Kaubamaja pakiautomaat',
          'address' => 
          array (
            0 => 'Kesk tänav',
            1 => '10',
          ),
          'zip' => '96045',
        ),
        1 => 
        array (
          'name' => 'Põlva Meie kaupluse pakiautomaat',
          'address' => 
          array (
            0 => 'Lao tänav',
            1 => '14',
          ),
          'zip' => '96201',
        ),
        2 => 
        array (
          'name' => 'Põlva Selveri pakiautomaat',
          'address' => 
          array (
            0 => 'Jaama tänav',
            1 => '12',
          ),
          'zip' => '96073',
        ),
      ),
      'Vastse-Kuuste alevik' => 
      array (
        0 => 
        array (
          'name' => 'Vastse-Kuuste A ja O pakiautomaat',
          'address' => 
          array (
            0 => 'Kesktänav',
            1 => '16',
          ),
          'zip' => '96322',
        ),
      ),
    ),
    'Räpina vald' => 
    array (
      'Räpina linn' => 
      array (
        0 => 
        array (
          'name' => 'Räpina Konsumi pakiautomaat',
          'address' => 
          array (
            0 => 'Kooli tänav',
            1 => '14a',
          ),
          'zip' => '96186',
        ),
        1 => 
        array (
          'name' => 'Räpina Maxima pakiautomaat',
          'address' => 
          array (
            0 => 'Pargi tänav',
            1 => '1',
          ),
          'zip' => '96095',
        ),
      ),
    ),
  ),
);