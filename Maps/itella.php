<?php
return array (
  'Tallinn/Harjumaa' => 
  array (
    'Tallinn' => 
    array (
      0 => 
      array (
        'name' => 'Kristiine Keskus 2.korrus',
        'address' => 'Endla 45',
        'place_id' => '227',
      ),
      1 => 
      array (
        'name' => 'Kakumäe Selver',
        'address' => 'Rannamõisa tee 6e, Tallinn',
        'place_id' => '213',
      ),
      2 => 
      array (
        'name' => 'Kristiine Keskus 1.korrus',
        'address' => 'Endla 45',
        'place_id' => '146',
      ),
      3 => 
      array (
        'name' => 'Priisle Maxima XXX',
        'address' => 'Linnamäe tee 57, Tallinn',
        'place_id' => '193',
      ),
      4 => 
      array (
        'name' => 'Pirita Keskus',
        'address' => 'Merivälja tee 24',
        'place_id' => '216',
      ),
      5 => 
      array (
        'name' => 'Balti Jaama Turg',
        'address' => 'Kopli 1, Tallinn',
        'place_id' => '208',
      ),
      6 => 
      array (
        'name' => 'Peterburi tee Rimi',
        'address' => 'Peterburi tee 98',
        'place_id' => '232',
      ),
      7 => 
      array (
        'name' => 'Muuga Maxima',
        'address' => 'Altmetsa 1, Muuga',
        'place_id' => '178',
      ),
      8 => 
      array (
        'name' => 'Loo Gross',
        'address' => 'Saha tee 9, Loo',
        'place_id' => '207',
      ),
      9 => 
      array (
        'name' => 'Kärberi Selver',
        'address' => 'K. Kärberi 20/20a, Tallinn',
        'place_id' => '199',
      ),
      10 => 
      array (
        'name' => 'Raudalu Konsum',
        'address' => 'Viljandi mnt. 41a, Tallinn',
        'place_id' => '174',
      ),
      11 => 
      array (
        'name' => 'Stockmann',
        'address' => 'Liivalaia 53, Tallinn',
        'place_id' => '200',
      ),
      12 => 
      array (
        'name' => 'Tallinna Arsenali Keskus',
        'address' => 'Erika 14, Tallinn',
        'place_id' => '225',
      ),
      13 => 
      array (
        'name' => 'Tallinna Astangu Rimi',
        'address' => 'Kotermaa tn 4, Tallinn',
        'place_id' => '228',
      ),
      14 => 
      array (
        'name' => 'Tallinna Finest ärimaja',
        'address' => 'Pärnu mnt 22',
        'place_id' => '234',
      ),
      15 => 
      array (
        'name' => 'Tallinna Haabersti Rimi',
        'address' => 'Haabersti 1',
        'place_id' => '121',
      ),
      16 => 
      array (
        'name' => 'Tallinna Hiiu Rimi',
        'address' => 'Pärnu mnt. 386',
        'place_id' => '214',
      ),
      17 => 
      array (
        'name' => 'Tallinna Järve Keskus',
        'address' => 'Pärnu mnt. 238',
        'place_id' => '126',
      ),
      18 => 
      array (
        'name' => 'Tallinna Kadaka Selver',
        'address' => 'Kadaka tee 56A, Tallinn',
        'place_id' => '185',
      ),
      19 => 
      array (
        'name' => 'Tallinna Kaubamaja',
        'address' => 'Gonsiori 2',
        'place_id' => '160',
      ),
      20 => 
      array (
        'name' => 'Tallinna Kolde Selver',
        'address' => 'Sõle tn 31',
        'place_id' => '233',
      ),
      21 => 
      array (
        'name' => 'Tallinna Lasnamäe Centrum (RIMI)',
        'address' => 'Mustakivi tee 13',
        'place_id' => '120',
      ),
      22 => 
      array (
        'name' => 'Tallinna Lasnamäe Maksimarket',
        'address' => 'J. Smuuli tee 43',
        'place_id' => '220',
      ),
      23 => 
      array (
        'name' => 'Tallinna Lasnamäe Prisma',
        'address' => 'Mustakivi tee 17',
        'place_id' => '103',
      ),
      24 => 
      array (
        'name' => 'Tallinna Läänemere Selver',
        'address' => 'Tallinn, Läänemere tee 28',
        'place_id' => '175',
      ),
      25 => 
      array (
        'name' => 'Tallinna Magistrali Keskus',
        'address' => 'Sõpruse pst 201/203',
        'place_id' => '164',
      ),
      26 => 
      array (
        'name' => 'Tallinna Marja Gross',
        'address' => 'Mustamäe tee 41, Tallinn',
        'place_id' => '206',
      ),
      27 => 
      array (
        'name' => 'Tallinna Merimetsa Selver',
        'address' => 'Paldiski mnt 56',
        'place_id' => '102',
      ),
      28 => 
      array (
        'name' => 'Tallinna Mustamäe Keskus',
        'address' => 'A. H. Tammsaare tee 104a, Tallinn',
        'place_id' => '195',
      ),
      29 => 
      array (
        'name' => 'Tallinna Mustika Prisma',
        'address' => 'Tammsaare tee 116 ',
        'place_id' => '138',
      ),
      30 => 
      array (
        'name' => 'Tallinna Nautica Keskus',
        'address' => 'Ahtri 9, Tallinn',
        'place_id' => '230',
      ),
      31 => 
      array (
        'name' => 'Tallinna Nõmme Keskus',
        'address' => 'Jaama 2',
        'place_id' => '124',
      ),
      32 => 
      array (
        'name' => 'Tallinna Pirita Selver',
        'address' => 'Rummu tee 4',
        'place_id' => '125',
      ),
      33 => 
      array (
        'name' => 'Tallinna Rocca Al Mare Keskus',
        'address' => 'Paldiski mnt 102',
        'place_id' => '153',
      ),
      34 => 
      array (
        'name' => 'Tallinna Rotermanni Keskus',
        'address' => 'Rotermanni 8, Tallinn',
        'place_id' => '222',
      ),
      35 => 
      array (
        'name' => 'Tallinna Sikupilli Prisma',
        'address' => 'Tartu mnt 87',
        'place_id' => '154',
      ),
      36 => 
      array (
        'name' => 'Tallinna Smuuli Maxima',
        'address' => 'Smuuli tee 9',
        'place_id' => '141',
      ),
      37 => 
      array (
        'name' => 'Tallinna Solaris Keskus',
        'address' => 'Estonia pst. 9, Tallinn',
        'place_id' => '155',
      ),
      38 => 
      array (
        'name' => 'Tallinna Stroomi Keskus',
        'address' => 'Tuulemaa 20, Tallinn',
        'place_id' => '188',
      ),
      39 => 
      array (
        'name' => 'Tallinna T1 Kaubanduskeskus',
        'address' => 'Peterburi tee 2',
        'place_id' => '226',
      ),
      40 => 
      array (
        'name' => 'Tallinna Tondi Selver',
        'address' => 'Tammsaare tee 62',
        'place_id' => '137',
      ),
      41 => 
      array (
        'name' => 'Tallinna Torupilli Selver',
        'address' => 'Vesivärava 37',
        'place_id' => '122',
      ),
      42 => 
      array (
        'name' => 'Tallinna Tähesaju Selver',
        'address' => 'Tähesaju tee 1, Tallinn',
        'place_id' => '204',
      ),
      43 => 
      array (
        'name' => 'Tallinna Viru Keskus',
        'address' => 'Viru väljak 4/6',
        'place_id' => '215',
      ),
      44 => 
      array (
        'name' => 'Tallinna Ülemiste keskus',
        'address' => 'Suur-Sõjamäe 4, Tallinn',
        'place_id' => '187',
      ),
      45 => 
      array (
        'name' => 'Vilde tee Maxima XX',
        'address' => 'Vilde tee 77, Tallinn',
        'place_id' => '194',
      ),
    ),
    'Jüri' => 
    array (
      0 => 
      array (
        'name' => 'Jüri Konsum',
        'address' => 'Aruküla tee 29, Jüri',
        'place_id' => '183',
      ),
    ),
    'Keila' => 
    array (
      0 => 
      array (
        'name' => 'Keila Grossi Toidukaubad',
        'address' => 'Piiri 7, Keila',
        'place_id' => '217',
      ),
      1 => 
      array (
        'name' => 'Keila Selver',
        'address' => 'Piiri 12',
        'place_id' => '134',
      ),
    ),
    'Kuusalu' => 
    array (
      0 => 
      array (
        'name' => 'Kuusalu Grossi Toidukaubad',
        'address' => 'Mäe 1, Kuusalu',
        'place_id' => '205',
      ),
    ),
    'Harjumaa' => 
    array (
      0 => 
      array (
        'name' => 'Peetri Selver',
        'address' => 'Veesaare tee 2, Rae vald',
        'place_id' => '179',
      ),
    ),
    'Maardu' => 
    array (
      0 => 
      array (
        'name' => 'Maardu Pärli Keskus',
        'address' => 'Nurga 3, Maardu',
        'place_id' => '219',
      ),
      1 => 
      array (
        'name' => 'Maardu Maxima',
        'address' => 'Keemikute 2',
        'place_id' => '161',
      ),
    ),
    'Laagri' => 
    array (
      0 => 
      array (
        'name' => 'Laagri Selver',
        'address' => 'Pärnu mnt. 554, Tallinn',
        'place_id' => '203',
      ),
      1 => 
      array (
        'name' => 'Laagri Maksimarket',
        'address' => 'Pärnu mnt 558a',
        'place_id' => '133',
      ),
    ),
    'Saku' => 
    array (
      0 => 
      array (
        'name' => 'Saku Selver',
        'address' => 'Üksnurme tee 2',
        'place_id' => '156',
      ),
    ),
    'Saue' => 
    array (
      0 => 
      array (
        'name' => 'Saue Maxima',
        'address' => 'Ladva 1a, Saue',
        'place_id' => '177',
      ),
    ),
    'Tabasalu' => 
    array (
      0 => 
      array (
        'name' => 'Tabasalu Rimi',
        'address' => 'Klooga mnt 10b, Tabasalu',
        'place_id' => '180',
      ),
    ),
    'Viimsi' => 
    array (
      0 => 
      array (
        'name' => 'Viimsi Kaubanduskeskus',
        'address' => 'Randvere tee 6',
        'place_id' => '139',
      ),
      1 => 
      array (
        'name' => 'Viimsi Keskus (Selver)',
        'address' => 'Sõpruse tee 15, Haabneeme alevik, Viimsi',
        'place_id' => '192',
      ),
    ),
  ),
  'Saaremaa' => 
  array (
    'Kuressaare' => 
    array (
      0 => 
      array (
        'name' => 'Kuressaare Kihelkonna mini-Rimi',
        'address' => 'Kihelkonna mnt. 3',
        'place_id' => '157',
      ),
      1 => 
      array (
        'name' => 'Auriga Keskus',
        'address' => 'Tallinna 88, Kudjape',
        'place_id' => '223',
      ),
      2 => 
      array (
        'name' => 'Kuressaare Saare Selver',
        'address' => 'Tallinna 67',
        'place_id' => '116',
      ),
    ),
  ),
  'Jõgevamaa' => 
  array (
    'Jõgeva' => 
    array (
      0 => 
      array (
        'name' => 'Jõgeva Kaubahall',
        'address' => 'Aia 3',
        'place_id' => '111',
      ),
      1 => 
      array (
        'name' => 'Jõgeva Pae Konsum',
        'address' => 'Aia 33, Jõgeva',
        'place_id' => '202',
      ),
    ),
    'Põltsamaa' => 
    array (
      0 => 
      array (
        'name' => 'Põltsamaa Selver',
        'address' => 'Jõgeva mnt. 1a, Põltsamaa',
        'place_id' => '123',
      ),
    ),
  ),
  'Hiiumaa' => 
  array (
    'Kärdla' => 
    array (
      0 => 
      array (
        'name' => 'Kärdla Selver',
        'address' => 'Rehemäe, Linnumäe küla, Pühalepa vald',
        'place_id' => '115',
      ),
    ),
  ),
  'Ida-Virumaa' => 
  array (
    'Kohtla-Järve' => 
    array (
      0 => 
      array (
        'name' => 'Ahtme Maxima',
        'address' => 'Puru tee 77, Ahtme',
        'place_id' => '198',
      ),
      1 => 
      array (
        'name' => 'Kohtla-Järve Vironia Keskus ',
        'address' => 'Järveküla tee 50',
        'place_id' => '131',
      ),
    ),
    'Sillamäe' => 
    array (
      0 => 
      array (
        'name' => 'Sillamäe SK-Market (Konsum)',
        'address' => 'Viru pst.2a',
        'place_id' => '140',
      ),
    ),
    'Narva' => 
    array (
      0 => 
      array (
        'name' => 'Narva Prisma',
        'address' => 'Kangelaste prospekt 29, Narva',
        'place_id' => '181',
      ),
      1 => 
      array (
        'name' => 'Narva Astri Keskus',
        'address' => 'Tallinna mnt 41',
        'place_id' => '109',
      ),
    ),
    'Jõhvi' => 
    array (
      0 => 
      array (
        'name' => 'Jõhvi Tsentraal',
        'address' => 'Keskväljak 4',
        'place_id' => '108',
      ),
      1 => 
      array (
        'name' => 'Jõhvi Grossi Toidukaubad',
        'address' => 'Tartu mnt 15a, Jõhvi',
        'place_id' => '184',
      ),
    ),
  ),
  'Järvamaa' => 
  array (
    'Türi' => 
    array (
      0 => 
      array (
        'name' => 'Türi mini-Rimi',
        'address' => 'Paide 26',
        'place_id' => '143',
      ),
    ),
    'Paide' => 
    array (
      0 => 
      array (
        'name' => 'Paide Maksimarket',
        'address' => 'Ringtee 2, Paide',
        'place_id' => '182',
      ),
      1 => 
      array (
        'name' => 'Paide Selver',
        'address' => 'Aiavilja 4',
        'place_id' => '112',
      ),
    ),
  ),
  'Pärnumaa' => 
  array (
    'Pärnu' => 
    array (
      0 => 
      array (
        'name' => 'Pärnu Keskus',
        'address' => 'Aida 7, Pärnu',
        'place_id' => '130',
      ),
      1 => 
      array (
        'name' => 'Pärnu Kaubamajakas',
        'address' => 'Papiniidu 8',
        'place_id' => '117',
      ),
      2 => 
      array (
        'name' => 'Pärnu Mai Selver',
        'address' => 'Papiniidu 42',
        'place_id' => '189',
      ),
      3 => 
      array (
        'name' => 'Pärnu Maksimarket',
        'address' => 'Pärnumaa, Audru vald, Haapsalu mnt 43',
        'place_id' => '173',
      ),
      4 => 
      array (
        'name' => 'Pärnu Maxima XXX',
        'address' => 'Riia mnt 131',
        'place_id' => '158',
      ),
      5 => 
      array (
        'name' => 'Pärnu Port Artur 2',
        'address' => 'Lai 11',
        'place_id' => '224',
      ),
      6 => 
      array (
        'name' => 'Pärnu Ülejõe Selver',
        'address' => 'Tallinna mnt 93a',
        'place_id' => '127',
      ),
    ),
    'Vändra' => 
    array (
      0 => 
      array (
        'name' => 'Vändra Konsum',
        'address' => 'Pärnu-Paide mnt. 21',
        'place_id' => '142',
      ),
    ),
  ),
  'Põlvamaa' => 
  array (
    'Põlva' => 
    array (
      0 => 
      array (
        'name' => 'Põlva Edu Keskus',
        'address' => 'Aasa 1',
        'place_id' => '128',
      ),
    ),
  ),
  'Lääne-Virumaa' => 
  array (
    'Tapa' => 
    array (
      0 => 
      array (
        'name' => 'Tapa Bussijaam',
        'address' => 'Sauna 1',
        'place_id' => '147',
      ),
      1 => 
      array (
        'name' => 'Tapa Konsum',
        'address' => 'Kalmistu 3',
        'place_id' => '229',
      ),
    ),
    'Rakvere' => 
    array (
      0 => 
      array (
        'name' => 'Rakvere Põhjakeskus',
        'address' => 'Tõrremäe, Rakvere vald',
        'place_id' => '152',
      ),
      1 => 
      array (
        'name' => 'Rakvere Kroonikeskus',
        'address' => 'F. G. Adoffi 11',
        'place_id' => '110',
      ),
      2 => 
      array (
        'name' => 'Rakvere Vaala Keskus',
        'address' => 'Lõõtspilli 2, Rakvere',
        'place_id' => '221',
      ),
    ),
  ),
  'Raplamaa' => 
  array (
    'Rapla' => 
    array (
      0 => 
      array (
        'name' => 'Rapla Selver',
        'address' => 'Tallinna maantee 4, Rapla',
        'place_id' => '190',
      ),
      1 => 
      array (
        'name' => 'Rapla Maxima',
        'address' => 'Tallinna mnt 50a',
        'place_id' => '113',
      ),
    ),
    'Märjamaa' => 
    array (
      0 => 
      array (
        'name' => 'Märjamaa Konsum',
        'address' => 'Pärnu mnt 62',
        'place_id' => '145',
      ),
    ),
  ),
  'Tartumaa' => 
  array (
    'Tartu' => 
    array (
      0 => 
      array (
        'name' => 'Tartu Tasku moe- ja vabaajakeskus',
        'address' => 'Turu 2',
        'place_id' => '135',
      ),
      1 => 
      array (
        'name' => 'Tartu Aardla Selver',
        'address' => 'Võru 77',
        'place_id' => '172',
      ),
      2 => 
      array (
        'name' => 'Tartu Anne Prisma',
        'address' => 'Nõlvaku tee 2, Tartu',
        'place_id' => '171',
      ),
      3 => 
      array (
        'name' => 'Tartu Anne Selver',
        'address' => 'Kalda tee 43',
        'place_id' => '106',
      ),
      4 => 
      array (
        'name' => 'Tartu Eeden',
        'address' => 'Kalda tee 1c',
        'place_id' => '150',
      ),
      5 => 
      array (
        'name' => 'Tartu Kaubamaja   -1 korrus',
        'address' => 'Riia 1, Tartu',
        'place_id' => '201',
      ),
      6 => 
      array (
        'name' => 'Tartu Kaubamaja 0 korrus',
        'address' => 'Riia 1, Tartu',
        'place_id' => '176',
      ),
      7 => 
      array (
        'name' => 'Tartu Kvartal',
        'address' => 'Riia 2, Tartu',
        'place_id' => '210',
      ),
      8 => 
      array (
        'name' => 'Tartu Lembitu Konsum',
        'address' => 'Lembitu tn. 2, Tartu',
        'place_id' => '166',
      ),
      9 => 
      array (
        'name' => 'Tartu Lõunakeskuse Rimi',
        'address' => 'Ringtee 75',
        'place_id' => '107',
      ),
      10 => 
      array (
        'name' => 'Tartu Raadi Maxima',
        'address' => 'Narva mnt 112/Peetri 26B',
        'place_id' => '163',
      ),
      11 => 
      array (
        'name' => 'Tartu Rebase Rimi',
        'address' => 'Rebase 10',
        'place_id' => '136',
      ),
      12 => 
      array (
        'name' => 'Tartu Ringtee Selver',
        'address' => 'Aardla 114, Tartu',
        'place_id' => '169',
      ),
      13 => 
      array (
        'name' => 'Tartu Saare Maxima',
        'address' => 'Anne 57',
        'place_id' => '211',
      ),
      14 => 
      array (
        'name' => 'Tartu Sõbrakeskus',
        'address' => 'Võru 55F, Tartu',
        'place_id' => '218',
      ),
      15 => 
      array (
        'name' => 'Tartu Vahi Selver',
        'address' => 'Vahi 62, Tartu',
        'place_id' => '212',
      ),
      16 => 
      array (
        'name' => 'Tartu Veeriku Selver',
        'address' => 'Vitamiini 1, Tartu',
        'place_id' => '196',
      ),
    ),
    'Elva' => 
    array (
      0 => 
      array (
        'name' => 'Elva Maxima',
        'address' => 'Valga maantee 5',
        'place_id' => '209',
      ),
      1 => 
      array (
        'name' => 'Elva mini-Rimi',
        'address' => 'Kesk 7',
        'place_id' => '144',
      ),
    ),
  ),
  'Läänemaa' => 
  array (
    'Uuemõisa' => 
    array (
      0 => 
      array (
        'name' => 'Rannarootsi Keskus',
        'address' => 'Rannarootsi tee 1',
        'place_id' => '114',
      ),
    ),
    'Haapsalu' => 
    array (
      0 => 
      array (
        'name' => 'Haapsalu Rimi',
        'address' => 'Jaama 32, Haapsalu',
        'place_id' => '167',
      ),
    ),
  ),
  'Valgamaa' => 
  array (
    'Otepää' => 
    array (
      0 => 
      array (
        'name' => 'Otepää Maxima',
        'address' => 'Valga mnt 1B',
        'place_id' => '149',
      ),
    ),
    'Tõrva' => 
    array (
      0 => 
      array (
        'name' => 'Tõrva Maxima',
        'address' => 'Tamme 2',
        'place_id' => '148',
      ),
    ),
    'Valga' => 
    array (
      0 => 
      array (
        'name' => 'Valga Selver',
        'address' => 'Raja 5',
        'place_id' => '118',
      ),
    ),
  ),
  'Viljandimaa' => 
  array (
    'Viljandi' => 
    array (
      0 => 
      array (
        'name' => 'Viljandi Männimäe Selver',
        'address' => 'Riia mnt 35',
        'place_id' => '101',
      ),
      1 => 
      array (
        'name' => 'Viljandi Turu Konsum',
        'address' => 'Turu 16, 71003, Viljandi linn',
        'place_id' => '191',
      ),
      2 => 
      array (
        'name' => 'Viljandi Uku keskus',
        'address' => 'Tallinna tn. 41',
        'place_id' => '162',
      ),
    ),
  ),
  'Võrumaa' => 
  array (
    'Võru' => 
    array (
      0 => 
      array (
        'name' => 'Võru Kagukeskus',
        'address' => 'Kooli 6',
        'place_id' => '119',
      ),
      1 => 
      array (
        'name' => 'Võru Rimi',
        'address' => 'Jüri 85',
        'place_id' => '168',
      ),
    ),
  ),
);