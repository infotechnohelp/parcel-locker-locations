<?php
return array (
  'Lehmja' => 
  array (
    0 => 
    array (
      'name' => 'DPD Eesti AS',
      'address' => 'Taevavärava tee 1',
      'zip' => '75306',
      'place_id' => '429047',
    ),
  ),
  'Saku' => 
  array (
    0 => 
    array (
      'name' => 'Tammemäe Alexela pakipood',
      'address' => 'Tammemäe ',
      'zip' => '75501',
      'place_id' => '429053',
    ),
    1 => 
    array (
      'name' => 'Automaat Saku Selver',
      'address' => 'Üksnurme tee 2 ',
      'zip' => '75501',
      'place_id' => '495980',
    ),
  ),
  'Paldiski' => 
  array (
    0 => 
    array (
      'name' => 'Paldiski Alexela pakipood',
      'address' => 'Tallinna mnt 20',
      'zip' => '76806',
      'place_id' => '429054',
    ),
  ),
  'Jüri' => 
  array (
    0 => 
    array (
      'name' => 'Jüri Alexela pakipood',
      'address' => 'Aleviku tee 5',
      'zip' => '75301',
      'place_id' => '429055',
    ),
  ),
  'Tallinn' => 
  array (
    0 => 
    array (
      'name' => 'Tallinna Idakeskuse Arvotek pakipoo',
      'address' => 'Punane 16',
      'zip' => '13619',
      'place_id' => '429066',
    ),
    1 => 
    array (
      'name' => 'Tallinna Bussijaama Cargobus pakipo',
      'address' => 'Lastekodu 46',
      'zip' => '10144',
      'place_id' => '429080',
    ),
    2 => 
    array (
      'name' => 'Nurmenuku keskuse ATF pakipood',
      'address' => 'Õismäe tee 1a/1b',
      'zip' => '13514',
      'place_id' => '429126',
    ),
    3 => 
    array (
      'name' => 'Tallinna Nõmme Loodusand pakipood',
      'address' => 'Jaama 3',
      'zip' => '11621',
      'place_id' => '429104',
    ),
    4 => 
    array (
      'name' => 'Tallinna Pärnu mnt Büroomaailm paki',
      'address' => 'Pärnu mnt 139C/1',
      'zip' => '11317',
      'place_id' => '429107',
    ),
    5 => 
    array (
      'name' => 'Automaat Tallinna Õismäe Maxima XX',
      'address' => 'Õismäe tee 46 ',
      'zip' => '13512',
      'place_id' => '461552',
    ),
    6 => 
    array (
      'name' => 'Automaat Tallinna Kristiine keskus',
      'address' => 'Endla 45 ',
      'zip' => '10615',
      'place_id' => '461553',
    ),
    7 => 
    array (
      'name' => 'Automaat Tallinna Lasnamäe Prisma',
      'address' => 'Mustakivi tee 17 ',
      'zip' => '13912',
      'place_id' => '461554',
    ),
    8 => 
    array (
      'name' => 'Automaat Tallinna Mustika keskus',
      'address' => 'Karjavälja 4 ',
      'zip' => '12918',
      'place_id' => '461555',
    ),
    9 => 
    array (
      'name' => 'Automaat Tallinna Stroomi keskus',
      'address' => 'Tuulemaa 20 ',
      'zip' => '10315',
      'place_id' => '461556',
    ),
    10 => 
    array (
      'name' => 'Automaat Tallinn Viru bussiterminal',
      'address' => 'Viru väljak 4 ',
      'zip' => '10111',
      'place_id' => '461557',
    ),
    11 => 
    array (
      'name' => 'Automaat Tallinna Haabersti Rimi',
      'address' => 'Haabersti 1 ',
      'zip' => '13517',
      'place_id' => '461559',
    ),
    12 => 
    array (
      'name' => 'Automaat Tallinna Magistrali keskus',
      'address' => 'Sõpruse pst 201/203 ',
      'zip' => '13419',
      'place_id' => '461560',
    ),
    13 => 
    array (
      'name' => 'Automaat Tallinna Järve keskus',
      'address' => 'Pärnu mnt 238 ',
      'zip' => '11624',
      'place_id' => '461561',
    ),
    14 => 
    array (
      'name' => 'Automaat Tallinna Linnamäe Maxima',
      'address' => 'Linnamäe tee 57 ',
      'zip' => '13911',
      'place_id' => '461569',
    ),
    15 => 
    array (
      'name' => 'Raudalu Konsumi Caramira pakipood',
      'address' => 'Viljandi mnt 41a',
      'zip' => '11218',
      'place_id' => '463412',
    ),
    16 => 
    array (
      'name' => 'Rocca al Mare Keskuse Klick pakipoo',
      'address' => 'Paldiski maantee 102',
      'zip' => '13522',
      'place_id' => '477096',
    ),
    17 => 
    array (
      'name' => 'Ülemiste Keskuse Klick pakipood',
      'address' => 'Suur-Sõjamäe 4',
      'zip' => '11415',
      'place_id' => '477161',
    ),
    18 => 
    array (
      'name' => 'Ümera Keskuse Mobiiliabi pakipood',
      'address' => 'Laagna tee 80',
      'zip' => '10115',
      'place_id' => '478168',
    ),
    19 => 
    array (
      'name' => 'Automaat robot Pirita Selver',
      'address' => 'Rummu tee 4 ',
      'zip' => '11911',
      'place_id' => '480990',
    ),
    20 => 
    array (
      'name' => 'Automaat robot Telliskivi Depoo',
      'address' => 'Telliskivi 64 ',
      'zip' => '10412',
      'place_id' => '480991',
    ),
    21 => 
    array (
      'name' => 'Automaat robot Kadaka Selver',
      'address' => 'Kadaka tee 56a ',
      'zip' => '12915',
      'place_id' => '480992',
    ),
    22 => 
    array (
      'name' => 'Automaat Tallinna Tondi Selver',
      'address' => 'A. H. Tammsaare tee 62 ',
      'zip' => '11316',
      'place_id' => '480677',
    ),
    23 => 
    array (
      'name' => 'Automaat Merimetsa Selver',
      'address' => 'Paldiski maantee 56 ',
      'zip' => '10617',
      'place_id' => '480678',
    ),
    24 => 
    array (
      'name' => 'Solarise Pärli kaupluse pakipood',
      'address' => 'Estonia pst. 9',
      'zip' => '10143',
      'place_id' => '480769',
    ),
    25 => 
    array (
      'name' => 'Automaat robot Sikupilli keskus',
      'address' => 'Tartu maantee 87 ',
      'zip' => '10112',
      'place_id' => '481903',
    ),
    26 => 
    array (
      'name' => 'Automaat Tallinna Torupilli Selver',
      'address' => 'Vesivärava 37 ',
      'zip' => '10126',
      'place_id' => '495983',
    ),
    27 => 
    array (
      'name' => 'Automaat Tallinna Arsenali keskus',
      'address' => 'Erika 14 ',
      'zip' => '10416',
      'place_id' => '496384',
    ),
    28 => 
    array (
      'name' => 'Automaat Tallinna Smuuli Maxima XX',
      'address' => 'J. Smuuli tee 9 ',
      'zip' => '13628',
      'place_id' => '496422',
    ),
    29 => 
    array (
      'name' => 'Automaat Tallinna Lasnamäe Centrum',
      'address' => 'Mustakivi tee 13 ',
      'zip' => '13912',
      'place_id' => '499198',
    ),
    30 => 
    array (
      'name' => 'Automaat Lasnamäe Maksimarket',
      'address' => 'J. Smuuli tee 43 ',
      'zip' => '11415',
      'place_id' => '499199',
    ),
    31 => 
    array (
      'name' => 'Automaat Tallinn Marienthali Selver',
      'address' => 'Mustamäe tee 16 ',
      'zip' => '10617',
      'place_id' => '499319',
    ),
    32 => 
    array (
      'name' => 'Automaat Tallinna Keskturu Maxima',
      'address' => 'Tartu mnt 49 ',
      'zip' => '10115',
      'place_id' => '499341',
    ),
    33 => 
    array (
      'name' => 'Automaat Tallinna Läänemere Selver',
      'address' => 'Läänemere tee 28 ',
      'zip' => '13913',
      'place_id' => '499346',
    ),
    34 => 
    array (
      'name' => 'Automaat Tallinna Põhja Rimi',
      'address' => 'Põhja pst 17 ',
      'zip' => '10414',
      'place_id' => '500128',
    ),
    35 => 
    array (
      'name' => 'Automaat Tallinna Hiiu Rimi',
      'address' => 'Pärnu mnt 386 ',
      'zip' => '11612',
      'place_id' => '500716',
    ),
    36 => 
    array (
      'name' => 'Automaat Tallinna Vineeri peatus',
      'address' => 'Pärnu mnt 67a ',
      'zip' => '10134',
      'place_id' => '500742',
    ),
    37 => 
    array (
      'name' => 'Automaat Tallinna Sõpruse Rimi',
      'address' => 'Sõpruse pst 174 ',
      'zip' => '13424',
      'place_id' => '500748',
    ),
    38 => 
    array (
      'name' => 'Automaat Tallinna Kolde Selver',
      'address' => 'Sõle 31 ',
      'zip' => '10321',
      'place_id' => '502856',
    ),
    39 => 
    array (
      'name' => 'Tallinna Maakri Copy Pro pakipood',
      'address' => 'Maakri 19/21 ',
      'zip' => '10145',
      'place_id' => '503195',
    ),
    40 => 
    array (
      'name' => 'Starship Mustamäe robotkuller',
      'address' => 'Teaduspargi 8 ',
      'zip' => '12618',
      'place_id' => '503297',
    ),
    41 => 
    array (
      'name' => 'Automaat Tallinna Ehitajate Maxima',
      'address' => 'Ehitajate tee 148 ',
      'zip' => '13517',
      'place_id' => '508935',
    ),
    42 => 
    array (
      'name' => 'Automaat Tallinna Kakumäe Selver',
      'address' => 'Rannamõisa tee 6 ',
      'zip' => '13516',
      'place_id' => '508936',
    ),
    43 => 
    array (
      'name' => 'Automaat Tallinna Kärberi Keskus',
      'address' => 'Kärberi 20 ',
      'zip' => '13919',
      'place_id' => '509277',
    ),
    44 => 
    array (
      'name' => 'Automaat Tallinn Paasiku Grossipood',
      'address' => 'Paasiku 2a ',
      'zip' => '13915',
      'place_id' => '509278',
    ),
    45 => 
    array (
      'name' => 'Automaat Tallinna Akadeemia Konsum',
      'address' => 'Akadeemia tee 35 ',
      'zip' => '12618',
      'place_id' => '509390',
    ),
    46 => 
    array (
      'name' => 'Automaat Tallinna Vilde tee Maxima',
      'address' => 'E. Vilde tee 75/77 ',
      'zip' => '12911',
      'place_id' => '509433',
    ),
    47 => 
    array (
      'name' => 'Automaat Tallinna Magdaleena',
      'address' => 'Pärnu mnt 106 ',
      'zip' => '11312',
      'place_id' => '509450',
    ),
  ),
  'Kohila' => 
  array (
    0 => 
    array (
      'name' => 'Kohila Midi pakipood',
      'address' => 'Lõuna 2',
      'zip' => '79801',
      'place_id' => '429100',
    ),
    1 => 
    array (
      'name' => 'Automaat Kohila Grossi kauplus',
      'address' => 'Viljandi mnt 3a ',
      'zip' => '79805',
      'place_id' => '509279',
    ),
  ),
  'Käina' => 
  array (
    0 => 
    array (
      'name' => 'Käina Rauapoe pakipood',
      'address' => 'Hiiu maantee 18',
      'zip' => '92101',
      'place_id' => '429128',
    ),
  ),
  'Kuusalu' => 
  array (
    0 => 
    array (
      'name' => 'Kuusalu Perepoe pakipood',
      'address' => 'Kuusalu tee 20',
      'zip' => '74601',
      'place_id' => '429307',
    ),
  ),
  'Kehra' => 
  array (
    0 => 
    array (
      'name' => 'Kehra Perepoe pakipood',
      'address' => 'Kose mnt 3a',
      'zip' => '74305',
      'place_id' => '429309',
    ),
    1 => 
    array (
      'name' => 'Automaat Kehra Grossi kauplus',
      'address' => 'Kose mnt 7 ',
      'zip' => '74307',
      'place_id' => '508005',
    ),
  ),
  'Orissaare' => 
  array (
    0 => 
    array (
      'name' => 'Orissaare Alexela pakipood',
      'address' => 'Kuivastu maantee 3a',
      'zip' => '94601',
      'place_id' => '452968',
    ),
  ),
  'Laagri' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Laagri Rimi',
      'address' => 'Pärnu mnt 556a ',
      'zip' => '76401',
      'place_id' => '461558',
    ),
  ),
  'Keila' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Keila Selver',
      'address' => 'Piiri 12 ',
      'zip' => '76610',
      'place_id' => '461568',
    ),
  ),
  'Viimsi' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Viimsi Selver',
      'address' => 'Sõpruse tee 15 ',
      'zip' => '74001',
      'place_id' => '461570',
    ),
    1 => 
    array (
      'name' => 'Automaat Viimsi Maxima X',
      'address' => 'Pargi tee 22 ',
      'zip' => '74001',
      'place_id' => '508063',
    ),
  ),
  'Virtsu' => 
  array (
    0 => 
    array (
      'name' => 'Virtsu Konsumi pakipood',
      'address' => 'Tallinna mnt 13 ',
      'zip' => '90101',
      'place_id' => '470090',
    ),
  ),
  'Kuressaare' => 
  array (
    0 => 
    array (
      'name' => 'Auriga Keskuse Klick pakipood',
      'address' => 'Tallinna 88',
      'zip' => '93815',
      'place_id' => '480033',
    ),
    1 => 
    array (
      'name' => 'Automaat Kuressaare Selver',
      'address' => 'Tallinna 67 ',
      'zip' => '93815',
      'place_id' => '495890',
    ),
    2 => 
    array (
      'name' => 'Automaat Kuressaare Rae Konsum',
      'address' => 'Raekoja 10 ',
      'zip' => '93814',
      'place_id' => '509003',
    ),
  ),
  'Haapsalu' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Haapsalu Kaubamaja Konsum',
      'address' => 'Posti 41 ',
      'zip' => '90507',
      'place_id' => '480657',
    ),
  ),
  'Tartu' => 
  array (
    0 => 
    array (
      'name' => 'Automaat robot Tartu Eedeni keskus',
      'address' => 'Kalda tee 1 ',
      'zip' => '50703',
      'place_id' => '480993',
    ),
    1 => 
    array (
      'name' => 'Automaat robot Tartu Lõunakeskus',
      'address' => 'Ringtee 75 ',
      'zip' => '50501',
      'place_id' => '481298',
    ),
    2 => 
    array (
      'name' => 'Automaat Tartu Anne Prisma',
      'address' => 'Nõlvaku 2 ',
      'zip' => '50708',
      'place_id' => '461562',
    ),
    3 => 
    array (
      'name' => 'Automaat Tartu bussijaam',
      'address' => 'Turu 2 ',
      'zip' => '51004',
      'place_id' => '461563',
    ),
    4 => 
    array (
      'name' => 'Tartu Büroomaailm pakipood',
      'address' => 'Rebase 12a',
      'zip' => '50104',
      'place_id' => '429109',
    ),
    5 => 
    array (
      'name' => 'Automaat Tartu Veeriku Selver',
      'address' => 'Vitamiini 1 ',
      'zip' => '50412',
      'place_id' => '499458',
    ),
    6 => 
    array (
      'name' => 'Automaat Tartu Raadi Maxima',
      'address' => 'Narva maantee 112 ',
      'zip' => '50303',
      'place_id' => '496409',
    ),
    7 => 
    array (
      'name' => 'Automaat Tartu Ringtee Selver',
      'address' => 'Aardla 114 ',
      'zip' => '50415',
      'place_id' => '480823',
    ),
    8 => 
    array (
      'name' => 'Tartu DR Sanitaartehnika pakipood',
      'address' => 'Lembitu 6',
      'zip' => '50406',
      'place_id' => '479707',
    ),
    9 => 
    array (
      'name' => 'Anne Selveri Kalastussport pakipood',
      'address' => 'Kalda tee 43b',
      'zip' => '50707',
      'place_id' => '478767',
    ),
    10 => 
    array (
      'name' => 'Automaat Tartu Riiamäe Alexela',
      'address' => 'Era 2 ',
      'zip' => '51010',
      'place_id' => '508371',
    ),
    11 => 
    array (
      'name' => 'Automaat Tartu Anne Maxima XX',
      'address' => 'Kalda tee 15 ',
      'zip' => '50703',
      'place_id' => '508064',
    ),
    12 => 
    array (
      'name' => 'Automaat Tartu Rebase Rimi',
      'address' => 'Rebase 10 ',
      'zip' => '50104',
      'place_id' => '508062',
    ),
  ),
  'Uuemõisa' => 
  array (
    0 => 
    array (
      'name' => 'Haapsalu Rannarootsi Charloti pakip',
      'address' => 'Rannarootsi tee 1',
      'zip' => '90401',
      'place_id' => '480770',
    ),
  ),
  'Rakvere' => 
  array (
    0 => 
    array (
      'name' => 'Automaat robot Rakvere kesklinn',
      'address' => 'Eduard Vilde 6 ',
      'zip' => '44310',
      'place_id' => '481642',
    ),
    1 => 
    array (
      'name' => 'Automaat Rakvere Põhjakeskus',
      'address' => 'Haljala tee 4 ',
      'zip' => '44415',
      'place_id' => '495984',
    ),
  ),
  'Viljandi' => 
  array (
    0 => 
    array (
      'name' => 'Automaat robot Viljandi UKU keskus',
      'address' => 'Tallinna 41 ',
      'zip' => '71020',
      'place_id' => '481902',
    ),
    1 => 
    array (
      'name' => 'Automaat Viljandi Männimäe Selver',
      'address' => 'Riia mnt 35 ',
      'zip' => '71002',
      'place_id' => '500510',
    ),
  ),
  'Kose' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Kose Grossi kauplus',
      'address' => 'Kodu 2 ',
      'zip' => '75101',
      'place_id' => '495889',
    ),
  ),
  'Peetri' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Peetri Selver',
      'address' => 'Veesaare 2 ',
      'zip' => '75312',
      'place_id' => '495979',
    ),
  ),
  'Muhu' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Muhu Liiva Konsum',
      'address' => 'Liiva ',
      'zip' => '94701',
      'place_id' => '495936',
    ),
  ),
  'Tabasalu' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Tabasalu Rimi',
      'address' => 'Klooga Maantee 10b ',
      'zip' => '76901',
      'place_id' => '496313',
    ),
  ),
  'Rapla' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Rapla Selver',
      'address' => 'Tallinna maantee 4 ',
      'zip' => '79511',
      'place_id' => '496408',
    ),
  ),
  'Maardu' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Maardu Maxima XX',
      'address' => 'Keemikute 2 ',
      'zip' => '74116',
      'place_id' => '496421',
    ),
  ),
  'Lihula' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Lihula Ehituskeskus',
      'address' => 'Tallinna mnt 29 ',
      'zip' => '90303',
      'place_id' => '499218',
    ),
  ),
  'Saue' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Saue Maxima X',
      'address' => 'Ladva 1a ',
      'zip' => '76506',
      'place_id' => '499347',
    ),
  ),
  'Kärdla' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Kärdla Selver',
      'address' => 'Rehemäe ',
      'zip' => '92422',
      'place_id' => '500162',
    ),
  ),
  'Märjamaa' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Märjamaa Maxima X',
      'address' => 'Kasti tee 1/3 ',
      'zip' => '78302',
      'place_id' => '500661',
    ),
  ),
  'Pärnu' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Pärnu Kaubamajakas',
      'address' => 'Papiniidu 8 ',
      'zip' => '80010',
      'place_id' => '509270',
    ),
    1 => 
    array (
      'name' => 'Automaat Pärnu Maksimarket',
      'address' => 'Haapsalu mnt 43 ',
      'zip' => '88317',
      'place_id' => '500576',
    ),
    2 => 
    array (
      'name' => 'Automaat Pärnu Keskus',
      'address' => 'Lai 5 ',
      'zip' => '80011',
      'place_id' => '461566',
    ),
    3 => 
    array (
      'name' => 'Automaat Pärnu Maxima XXX',
      'address' => 'Riia mnt 131 ',
      'zip' => '80042',
      'place_id' => '461567',
    ),
  ),
  'Vändra' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Vändra Konsum',
      'address' => 'Pärnu-Paide mnt 21 ',
      'zip' => '87701',
      'place_id' => '508934',
    ),
  ),
  'Sindi' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Sindi Konsum',
      'address' => 'Jaama 8 ',
      'zip' => '86705',
      'place_id' => '500577',
    ),
  ),
  'Põlva' => 
  array (
    0 => 
    array (
      'name' => 'Põlva Edu keskuse Raudnagel pakipoo',
      'address' => 'Aasa 1',
      'zip' => '63308',
      'place_id' => '429102',
    ),
    1 => 
    array (
      'name' => 'Automaat Põlva Selver',
      'address' => 'Jaama 12 ',
      'zip' => '63303',
      'place_id' => '500350',
    ),
  ),
  'Elva' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Elva Maxima X',
      'address' => 'Valga mnt 5 ',
      'zip' => '61504',
      'place_id' => '499535',
    ),
  ),
  'Valga' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Valga Rimi',
      'address' => 'Riia 18 ',
      'zip' => '68203',
      'place_id' => '499602',
    ),
    1 => 
    array (
      'name' => 'Valga Maxima Klick pakipood',
      'address' => 'Jaama 2b',
      'zip' => '68204',
      'place_id' => '482267',
    ),
  ),
  'Räpina' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Räpina Maxima X',
      'address' => 'Pargi 1 ',
      'zip' => '64504',
      'place_id' => '500269',
    ),
  ),
  'Otepää' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Otepää Maxima',
      'address' => 'Valga mnt 1b ',
      'zip' => '67403',
      'place_id' => '500390',
    ),
  ),
  'Põltsamaa' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Põltsamaa Konsum',
      'address' => 'Kesk 6 ',
      'zip' => '48105',
      'place_id' => '500166',
    ),
  ),
  'Võru' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Võru Maxima XX',
      'address' => 'Kooli 2 ',
      'zip' => '65606',
      'place_id' => '499281',
    ),
    1 => 
    array (
      'name' => 'Võru Kagukeskuse Angelgold pakipood',
      'address' => 'Kooli 6',
      'zip' => '65606',
      'place_id' => '479706',
    ),
    2 => 
    array (
      'name' => 'Automaat Võru Maksimarket',
      'address' => 'Jüri 83 ',
      'zip' => '65607',
      'place_id' => '508204',
    ),
  ),
  'Jõgeva' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Jõgeva Pae Konsum',
      'address' => 'Aia 33 ',
      'zip' => '48304',
      'place_id' => '495981',
    ),
  ),
  'Tõrvandi' => 
  array (
    0 => 
    array (
      'name' => 'Tõrvandi Terminaal Oil pakipood',
      'address' => 'Kauba tee 4',
      'zip' => '61715',
      'place_id' => '471505',
    ),
  ),
  'Antsla' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Antsla A ja O',
      'address' => 'Jaani 4 ',
      'zip' => '66404',
      'place_id' => '500349',
    ),
  ),
  'Mustvee' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Mustvee Grossi kauplus',
      'address' => 'Tähe 9 ',
      'zip' => '49603',
      'place_id' => '508098',
    ),
  ),
  'Tõrva' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Tõrva Kevade keskus',
      'address' => 'Valga 3a ',
      'zip' => '68601',
      'place_id' => '509068',
    ),
  ),
  'Ülenurme' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Ülenurme Konsum',
      'address' => 'Võru mnt 2 ',
      'zip' => '61714',
      'place_id' => '509156',
    ),
  ),
  'Väike-Maarja' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Väike-Maarja Konsum',
      'address' => 'Pikk 12 ',
      'zip' => '46202',
      'place_id' => '508097',
    ),
  ),
  'Kunda' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Kunda Grossi kauplus',
      'address' => 'Kasemäe 12 ',
      'zip' => '44107',
      'place_id' => '495934',
    ),
  ),
  'Tapa' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Tapa Konsum',
      'address' => 'Kalmistu 3 ',
      'zip' => '45108',
      'place_id' => '499223',
    ),
  ),
  'Loksa' => 
  array (
    0 => 
    array (
      'name' => 'Loksa Perepoe pakipood',
      'address' => 'Papli 2',
      'zip' => '74806',
      'place_id' => '429308',
    ),
  ),
  'Narva' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Narva Kreenholmi Maxima XX',
      'address' => 'Kreenholmi 52 ',
      'zip' => '20104',
      'place_id' => '476787',
    ),
    1 => 
    array (
      'name' => 'Narva Fama Keskuse Klick pakipood',
      'address' => 'Tallinna maantee 19c',
      'zip' => '20303',
      'place_id' => '477095',
    ),
    2 => 
    array (
      'name' => 'Automaat Narva Prisma',
      'address' => 'Kangelaste prospekt 29 ',
      'zip' => '20607',
      'place_id' => '499377',
    ),
    3 => 
    array (
      'name' => 'Automaat Narva Astri keskus',
      'address' => 'Tallinna mnt 41 ',
      'zip' => '20605',
      'place_id' => '461564',
    ),
    4 => 
    array (
      'name' => 'Automaat Narva Tiimanni Maxima XX',
      'address' => 'Tiimanni 20 ',
      'zip' => '21004',
      'place_id' => '508150',
    ),
  ),
  'Ahtme' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Ahtme Maxima XX',
      'address' => 'Puru Tee 77 ',
      'zip' => '31023',
      'place_id' => '495935',
    ),
  ),
  'Kohtla-Järve' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Kohtla-Järve Vironia',
      'address' => 'Järveküla tee 50 ',
      'zip' => '30321',
      'place_id' => '496314',
    ),
  ),
  'Jõhvi' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Jõhvi Maxima XX',
      'address' => 'Rakvere 29 ',
      'zip' => '41533',
      'place_id' => '499366',
    ),
    1 => 
    array (
      'name' => 'Automaat Jõhvi Tsentraal',
      'address' => 'Keskväljak 4 ',
      'zip' => '41531',
      'place_id' => '461565',
    ),
  ),
  'Avinurme' => 
  array (
    0 => 
    array (
      'name' => 'Avinurme Puiduait pakipood',
      'address' => 'Võidu 3',
      'zip' => '42101',
      'place_id' => '504528',
    ),
  ),
  'Sillamäe' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Sillamäe Maxima XX',
      'address' => 'Ak. Pavlovi 1 ',
      'zip' => '40232',
      'place_id' => '500937',
    ),
  ),
  'Kiviõli' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Kiviõli K5 keskus',
      'address' => 'Metsa 2 ',
      'zip' => '43125',
      'place_id' => '508149',
    ),
  ),
  'Mustla' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Mustla Konsumi',
      'address' => 'Posti 52a ',
      'zip' => '69701',
      'place_id' => '509067',
    ),
  ),
  'Imavere' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Imavere Meie kauplus',
      'address' => 'Rukkilille ',
      'zip' => '72401',
      'place_id' => '509039',
    ),
  ),
  'Suure-Jaani' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Suure-Jaani Konsum',
      'address' => 'Pärnu 3 ',
      'zip' => '71502',
      'place_id' => '509040',
    ),
  ),
  'Abja-Paluoja' => 
  array (
    0 => 
    array (
      'name' => 'Abja-Paluoja Alexela pakipood',
      'address' => 'Raudtee 2',
      'zip' => '69403',
      'place_id' => '429061',
    ),
  ),
  'Paide' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Paide Konsum',
      'address' => 'Keskväljak 15 ',
      'zip' => '72711',
      'place_id' => '499365',
    ),
    1 => 
    array (
      'name' => 'Paide Maksimarketi Klick pakipood',
      'address' => 'Ringtee 2 ',
      'zip' => '72751',
      'place_id' => '478617',
    ),
  ),
  'Karksi-Nuia' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Karksi-Nuia Konsum',
      'address' => 'Rahumäe 1 ',
      'zip' => '69103',
      'place_id' => '500539',
    ),
  ),
  'Türi' => 
  array (
    0 => 
    array (
      'name' => 'Automaat Türi Konsum',
      'address' => 'Tallinna 4 ',
      'zip' => '72211',
      'place_id' => '495982',
    ),
  ),
);